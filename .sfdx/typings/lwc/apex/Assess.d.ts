declare module "@salesforce/apex/Assess.fetchPicklist" {
  export default function fetchPicklist(param: {objectName: any, fieldName: any}): Promise<any>;
}
declare module "@salesforce/apex/Assess.getConsumerNameFromRequest" {
  export default function getConsumerNameFromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/Assess.saveData" {
  export default function saveData(param: {dataObj: any}): Promise<any>;
}
