declare module "@salesforce/apex/AssessorResult.assessorResult" {
  export default function assessorResult(param: {requestId: any, assesserResult: any}): Promise<any>;
}
declare module "@salesforce/apex/AssessorResult.getAssessorResult" {
  export default function getAssessorResult(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/AssessorResult.checkAssessor" {
  export default function checkAssessor(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/AssessorResult.getCaseNumber" {
  export default function getCaseNumber(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/AssessorResult.getCasesFromAccount" {
  export default function getCasesFromAccount(): Promise<any>;
}
