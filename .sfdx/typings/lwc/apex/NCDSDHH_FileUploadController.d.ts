declare module "@salesforce/apex/NCDSDHH_FileUploadController.mapRelatedFields" {
  export default function mapRelatedFields(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NCDSDHH_FileUploadController.retrieveDocumentInfo" {
  export default function retrieveDocumentInfo(param: {fileName: any, objectId: any}): Promise<any>;
}
declare module "@salesforce/apex/NCDSDHH_FileUploadController.deleteDocumentName" {
  export default function deleteDocumentName(param: {documentId: any, fileName: any, objectId: any, fieldApiName: any, currentObjectId: any}): Promise<any>;
}
declare module "@salesforce/apex/NCDSDHH_FileUploadController.updateDocumentName" {
  export default function updateDocumentName(param: {documentId: any, fileName: any, objectId: any, fieldApiName: any, currentObjectId: any}): Promise<any>;
}
