declare module "@salesforce/apex/NC_ApplicantSignature.getEquipmentDetails" {
  export default function getEquipmentDetails(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.saveData" {
  export default function saveData(param: {dataObj: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.saveCommentData" {
  export default function saveCommentData(param: {dataObjComment: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.getConsumerNameFromRequest" {
  export default function getConsumerNameFromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.getTodaysDate" {
  export default function getTodaysDate(): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.fetchSignature" {
  export default function fetchSignature(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.getAccountFromRequest" {
  export default function getAccountFromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.getStatus" {
  export default function getStatus(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ApplicantSignature.fetchFields" {
  export default function fetchFields(param: {requestId: any}): Promise<any>;
}
