declare module "@salesforce/apex/NC_AssessmentCtrl.fetchPicklist" {
  export default function fetchPicklist(param: {objectName: any, fieldName: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.getConsumerNameFromRequest" {
  export default function getConsumerNameFromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.getAccountFromRequest" {
  export default function getAccountFromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.getTodaysDate" {
  export default function getTodaysDate(): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.saveData" {
  export default function saveData(param: {dataObj: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.saveCommentData" {
  export default function saveCommentData(param: {dataObjComment: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.changeStatus" {
  export default function changeStatus(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.fetchSignature" {
  export default function fetchSignature(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.getStatus" {
  export default function getStatus(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_AssessmentCtrl.fetchFields" {
  export default function fetchFields(param: {requestId: any}): Promise<any>;
}
