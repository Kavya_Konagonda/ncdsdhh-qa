declare module "@salesforce/apex/NC_CampaignMemberController.getCampgaignMember" {
  export default function getCampgaignMember(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_CampaignMemberController.createCampaignMember" {
  export default function createCampaignMember(param: {recordId: any, contactIds: any}): Promise<any>;
}
