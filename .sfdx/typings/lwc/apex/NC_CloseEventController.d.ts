declare module "@salesforce/apex/NC_CloseEventController.closeEvent" {
  export default function closeEvent(param: {recordId: any}): Promise<any>;
}
