declare module "@salesforce/apex/NC_CommunicationAccessRequestHomeCtrl.checkPermissions" {
  export default function checkPermissions(): Promise<any>;
}
declare module "@salesforce/apex/NC_CommunicationAccessRequestHomeCtrl.returnRecordTypeId" {
  export default function returnRecordTypeId(): Promise<any>;
}
declare module "@salesforce/apex/NC_CommunicationAccessRequestHomeCtrl.getResourceLoanRecordTypeId" {
  export default function getResourceLoanRecordTypeId(): Promise<any>;
}
