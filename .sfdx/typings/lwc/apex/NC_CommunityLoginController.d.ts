declare module "@salesforce/apex/NC_CommunityLoginController.logInUser" {
  export default function logInUser(param: {jsonString: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_CommunityLoginController.forgotPassword" {
  export default function forgotPassword(param: {jsonString: any}): Promise<any>;
}
