declare module "@salesforce/apex/NC_ConnectEquipmentController.getRecordTypes" {
  export default function getRecordTypes(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ConnectEquipmentController.getEquipmentCategories" {
  export default function getEquipmentCategories(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ConnectEquipmentController.retrieveEquipments" {
  export default function retrieveEquipments(): Promise<any>;
}
declare module "@salesforce/apex/NC_ConnectEquipmentController.createEquipmentAndEquipmentRequest" {
  export default function createEquipmentAndEquipmentRequest(param: {equipmentDetails: any, requestId: any}): Promise<any>;
}
