declare module "@salesforce/apex/NC_Constant.getRecordTypeForInvoice" {
  export default function getRecordTypeForInvoice(param: {recordId: any}): Promise<any>;
}
