declare module "@salesforce/apex/NC_CreateAppointmentController.retrieveVendor" {
  export default function retrieveVendor(param: {authorizationId: any}): Promise<any>;
}
