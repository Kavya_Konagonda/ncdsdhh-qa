declare module "@salesforce/apex/NC_CreateAssetRecordComponentHandler.cloneAssetRecord" {
  export default function cloneAssetRecord(param: {recordId: any}): Promise<any>;
}
