declare module "@salesforce/apex/NC_CreateAttendeeRecordController.retrieveDefaults" {
  export default function retrieveDefaults(param: {recordId: any}): Promise<any>;
}
