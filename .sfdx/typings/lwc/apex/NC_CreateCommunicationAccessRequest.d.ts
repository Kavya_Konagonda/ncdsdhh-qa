declare module "@salesforce/apex/NC_CreateCommunicationAccessRequest.retrieveDefaults" {
  export default function retrieveDefaults(param: {recordId: any, isNDBEDPRequest: any}): Promise<any>;
}
