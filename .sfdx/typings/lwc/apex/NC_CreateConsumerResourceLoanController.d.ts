declare module "@salesforce/apex/NC_CreateConsumerResourceLoanController.retrieveDefaults" {
  export default function retrieveDefaults(param: {recordId: any}): Promise<any>;
}
