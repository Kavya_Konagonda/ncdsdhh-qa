declare module "@salesforce/apex/NC_CreateContactAttemptController.retrieveDefaults" {
  export default function retrieveDefaults(param: {recordId: any}): Promise<any>;
}
