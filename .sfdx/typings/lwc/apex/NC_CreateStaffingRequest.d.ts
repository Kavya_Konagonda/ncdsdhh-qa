declare module "@salesforce/apex/NC_CreateStaffingRequest.retrieveDefaultsForInternal" {
  export default function retrieveDefaultsForInternal(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_CreateStaffingRequest.retrieveDefaultsForExternal" {
  export default function retrieveDefaultsForExternal(param: {recordId: any}): Promise<any>;
}
