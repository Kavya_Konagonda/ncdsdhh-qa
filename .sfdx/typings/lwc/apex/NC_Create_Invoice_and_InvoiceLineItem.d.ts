declare module "@salesforce/apex/NC_Create_Invoice_and_InvoiceLineItem.createInvoiceAndInvoiceLineItem" {
  export default function createInvoiceAndInvoiceLineItem(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_Create_Invoice_and_InvoiceLineItem.createTrainingVendorInvoice" {
  export default function createTrainingVendorInvoice(param: {vendorRecordTypeAuth: any, recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_Create_Invoice_and_InvoiceLineItem.createCommunicationInvoice" {
  export default function createCommunicationInvoice(param: {communicationrecordTypeAuth: any, recordId: any}): Promise<any>;
}
