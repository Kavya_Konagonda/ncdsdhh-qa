declare module "@salesforce/apex/NC_FetchEquipmentHistory.fetchEquipmentHistory" {
  export default function fetchEquipmentHistory(param: {contactId: any}): Promise<any>;
}
