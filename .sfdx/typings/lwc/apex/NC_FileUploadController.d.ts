declare module "@salesforce/apex/NC_FileUploadController.mapRelatedFields" {
  export default function mapRelatedFields(param: {recordId: any, fieldDisable: any, fieldValue: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_FileUploadController.retrieveDocumentInfo" {
  export default function retrieveDocumentInfo(param: {fileName: any, objectId: any, filter: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_FileUploadController.deleteDocumentName" {
  export default function deleteDocumentName(param: {documentId: any, fileName: any, objectId: any, fieldApiName: any, currentObjectId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_FileUploadController.updateDocumentName" {
  export default function updateDocumentName(param: {documentId: any, fileName: any, objectId: any, fieldApiName: any, currentObjectId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_FileUploadController.updateFileRelatedField" {
  export default function updateFileRelatedField(param: {chkbkVal: any, documentId: any, fileName: any, fieldApiName: any, currentObjectId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_FileUploadController.updateContentVersion" {
  export default function updateContentVersion(param: {uploadId: any, recordId: any, fileName: any, fieldApiName: any, currentObjectId: any}): Promise<any>;
}
