declare module "@salesforce/apex/NC_IncomeWorksheetController.fetchContactData" {
  export default function fetchContactData(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_IncomeWorksheetController.updateContactData" {
  export default function updateContactData(param: {familyJsonData: any, recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_IncomeWorksheetController.fetchPicklist" {
  export default function fetchPicklist(param: {objectName: any, fieldName: any}): Promise<any>;
}
