declare module "@salesforce/apex/NC_InformationSessionConsumerRequest.fetchConsumerList" {
  export default function fetchConsumerList(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_InformationSessionConsumerRequest.sendInviationToConsumer" {
  export default function sendInviationToConsumer(param: {consumerdata: any, recordId: any}): Promise<any>;
}
