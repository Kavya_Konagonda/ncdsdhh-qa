declare module "@salesforce/apex/NC_InvoiceLineItemNDBEDPController.getAllLineItems" {
  export default function getAllLineItems(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_InvoiceLineItemNDBEDPController.updateAllLineItems" {
  export default function updateAllLineItems(param: {lineItemData: any, recordId: any}): Promise<any>;
}
