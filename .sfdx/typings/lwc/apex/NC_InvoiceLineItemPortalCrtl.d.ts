declare module "@salesforce/apex/NC_InvoiceLineItemPortalCrtl.checkRecordType" {
  export default function checkRecordType(param: {invoiceId: any}): Promise<any>;
}
