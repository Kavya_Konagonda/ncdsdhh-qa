declare module "@salesforce/apex/NC_InvoiceLineItemStaffingController.getAllLineItems" {
  export default function getAllLineItems(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_InvoiceLineItemStaffingController.updateAllLineItems" {
  export default function updateAllLineItems(param: {lineItemData: any, recordId: any}): Promise<any>;
}
