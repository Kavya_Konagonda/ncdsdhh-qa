declare module "@salesforce/apex/NC_ItemsToApproveController.getSubmittedRecords" {
  export default function getSubmittedRecords(): Promise<any>;
}
declare module "@salesforce/apex/NC_ItemsToApproveController.processRecords" {
  export default function processRecords(param: {lstWorkItemIds: any, processType: any}): Promise<any>;
}
