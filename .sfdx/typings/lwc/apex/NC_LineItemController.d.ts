declare module "@salesforce/apex/NC_LineItemController.getAllLineItems" {
  export default function getAllLineItems(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_LineItemController.updateAllLineItems" {
  export default function updateAllLineItems(param: {lineItemData: any}): Promise<any>;
}
