declare module "@salesforce/apex/NC_LwcCustomPathController.getPicklistValues" {
  export default function getPicklistValues(param: {fieldApiName: any, recordId: any, recordTypesNames: any}): Promise<any>;
}
