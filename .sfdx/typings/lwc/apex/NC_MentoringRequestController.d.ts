declare module "@salesforce/apex/NC_MentoringRequestController.fetchPicklist" {
  export default function fetchPicklist(param: {objectName: any, fieldName: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_MentoringRequestController.createMentoringRequestRecord" {
  export default function createMentoringRequestRecord(param: {wizardData: any, recordId: any}): Promise<any>;
}
