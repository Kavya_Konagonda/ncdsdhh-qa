declare module "@salesforce/apex/NC_NDBEDPAssetReassignmentInfoController.getAssetReassignmentInfo" {
  export default function getAssetReassignmentInfo(param: {recordId: any}): Promise<any>;
}
