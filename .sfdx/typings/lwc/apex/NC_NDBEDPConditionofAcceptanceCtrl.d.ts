declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.saveData" {
  export default function saveData(param: {dataObj: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.fetchSignature" {
  export default function fetchSignature(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.getConsumerNameFromRequest" {
  export default function getConsumerNameFromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.getAccountFromRequest" {
  export default function getAccountFromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.getTodaysDate" {
  export default function getTodaysDate(): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.getStatus" {
  export default function getStatus(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.getAcceptedValuefromRequest" {
  export default function getAcceptedValuefromRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.fetchFields" {
  export default function fetchFields(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.saveCommentData" {
  export default function saveCommentData(param: {dataObjComment: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_NDBEDPConditionofAcceptanceCtrl.requestEquipmentsforRequest" {
  export default function requestEquipmentsforRequest(param: {requestId: any}): Promise<any>;
}
