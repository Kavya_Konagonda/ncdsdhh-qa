declare module "@salesforce/apex/NC_Open_DocumentController.getPageURL" {
  export default function getPageURL(param: {recordId: any}): Promise<any>;
}
