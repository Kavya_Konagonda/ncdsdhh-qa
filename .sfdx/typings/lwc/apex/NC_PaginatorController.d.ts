declare module "@salesforce/apex/NC_PaginatorController.retrieveRecords" {
  export default function retrieveRecords(param: {parameters: any}): Promise<any>;
}
