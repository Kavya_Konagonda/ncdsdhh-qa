declare module "@salesforce/apex/NC_RefreshLightningPageController.checkPageRefresh" {
  export default function checkPageRefresh(param: {recordId: any}): Promise<any>;
}
