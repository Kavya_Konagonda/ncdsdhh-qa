declare module "@salesforce/apex/NC_Request.getConsumerName" {
  export default function getConsumerName(param: {contactId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_Request.getRecordTypeId" {
  export default function getRecordTypeId(param: {requestRecordTypeName: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_Request.saveData" {
  export default function saveData(param: {dataObj: any}): Promise<any>;
}
