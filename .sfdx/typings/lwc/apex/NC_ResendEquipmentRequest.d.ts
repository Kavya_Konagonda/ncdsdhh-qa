declare module "@salesforce/apex/NC_ResendEquipmentRequest.getStatus" {
  export default function getStatus(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ResendEquipmentRequest.validateEquipment" {
  export default function validateEquipment(param: {requestId: any}): Promise<any>;
}
