declare module "@salesforce/apex/NC_ResourceTransferController.getRegionalCenter" {
  export default function getRegionalCenter(param: {id: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_ResourceTransferController.transferResourceToRegional" {
  export default function transferResourceToRegional(param: {id: any, regionalCenterId: any}): Promise<any>;
}
