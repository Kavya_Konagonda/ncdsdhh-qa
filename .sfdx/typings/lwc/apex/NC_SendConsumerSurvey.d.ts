declare module "@salesforce/apex/NC_SendConsumerSurvey.createSurveyInvitaions" {
  export default function createSurveyInvitaions(param: {recordId: any, contactIds: any, data: any}): Promise<any>;
}
