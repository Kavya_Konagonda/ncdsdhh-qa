declare module "@salesforce/apex/NC_SignatureController.saveSignature" {
  export default function saveSignature(param: {signatureBody: any, applicationId: any, customField: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_SignatureController.getImage" {
  export default function getImage(param: {applicationId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_SignatureController.checkFilter" {
  export default function checkFilter(param: {recordId: any, filter: any}): Promise<any>;
}
