declare module "@salesforce/apex/NC_StaffInvitationComponentController.fetchUserList" {
  export default function fetchUserList(param: {recordId: any, regionalCenter: any, County: any, licenseType: any, speciality: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_StaffInvitationComponentController.sendEmailInviation" {
  export default function sendEmailInviation(param: {userdata: any, recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_StaffInvitationComponentController.fetchCountRegionalCenter" {
  export default function fetchCountRegionalCenter(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_StaffInvitationComponentController.fetchPicklist" {
  export default function fetchPicklist(param: {objectName: any, fieldName: any}): Promise<any>;
}
