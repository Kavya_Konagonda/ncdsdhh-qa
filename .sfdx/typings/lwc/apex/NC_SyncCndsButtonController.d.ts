declare module "@salesforce/apex/NC_SyncCndsButtonController.getCNDSSyncStatus" {
  export default function getCNDSSyncStatus(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_SyncCndsButtonController.getCommentFromError" {
  export default function getCommentFromError(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_SyncCndsButtonController.sendLookupRequest" {
  export default function sendLookupRequest(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_SyncCndsButtonController.sendUpdateRequest" {
  export default function sendUpdateRequest(param: {recordId: any}): Promise<any>;
}
