declare module "@salesforce/apex/NC_SyncNDBEDPButtonController.getNDBEDPSyncStatus" {
  export default function getNDBEDPSyncStatus(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_SyncNDBEDPButtonController.getCommentFromError" {
  export default function getCommentFromError(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/NC_SyncNDBEDPButtonController.sendLookupRequest" {
  export default function sendLookupRequest(param: {recordId: any}): Promise<any>;
}
