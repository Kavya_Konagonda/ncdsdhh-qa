declare module "@salesforce/apex/NC_VendorDocumentManagerController.retrieveVendorFilesList" {
  export default function retrieveVendorFilesList(param: {accountId: any}): Promise<any>;
}
