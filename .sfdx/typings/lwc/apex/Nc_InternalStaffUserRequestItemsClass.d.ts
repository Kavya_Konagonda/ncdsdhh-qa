declare module "@salesforce/apex/Nc_InternalStaffUserRequestItemsClass.updateAllStaffItems" {
  export default function updateAllStaffItems(param: {recordId: any, internalStaffItemList: any, totalAmountBilled: any}): Promise<any>;
}
declare module "@salesforce/apex/Nc_InternalStaffUserRequestItemsClass.getAllInternalStaffItems" {
  export default function getAllInternalStaffItems(param: {recordId: any}): Promise<any>;
}
