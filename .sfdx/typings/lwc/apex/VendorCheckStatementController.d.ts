declare module "@salesforce/apex/VendorCheckStatementController.retrieveCheckStatements" {
  export default function retrieveCheckStatements(param: {vendorId: any, offset: any, recordLimit: any, searchFilter: any, month: any, year: any, isPrinted: any, isMailed: any, isElectronic: any}): Promise<any>;
}
declare module "@salesforce/apex/VendorCheckStatementController.markStatementPrintedAndMailed" {
  export default function markStatementPrintedAndMailed(param: {checkStatementId: any, markPrinted: any, markMailed: any}): Promise<any>;
}
