declare module "@salesforce/apex/nc_DSDHHUnsubscribeController.getApplicationRecords" {
  export default function getApplicationRecords(param: {contactId: any}): Promise<any>;
}
declare module "@salesforce/apex/nc_DSDHHUnsubscribeController.saveData" {
  export default function saveData(param: {contactRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/nc_DSDHHUnsubscribeController.fatchMultiPicklist" {
  export default function fatchMultiPicklist(param: {objectName: any, fieldName: any}): Promise<any>;
}
