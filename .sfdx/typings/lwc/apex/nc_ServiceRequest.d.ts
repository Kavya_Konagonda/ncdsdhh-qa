declare module "@salesforce/apex/nc_ServiceRequest.checkNDBEDP" {
  export default function checkNDBEDP(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/nc_ServiceRequest.checkUserProfile" {
  export default function checkUserProfile(): Promise<any>;
}
declare module "@salesforce/apex/nc_ServiceRequest.getParentCaseNumber" {
  export default function getParentCaseNumber(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/nc_ServiceRequest.getParentCaseNumberId" {
  export default function getParentCaseNumberId(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/nc_ServiceRequest.saveData" {
  export default function saveData(param: {dataObj: any}): Promise<any>;
}
