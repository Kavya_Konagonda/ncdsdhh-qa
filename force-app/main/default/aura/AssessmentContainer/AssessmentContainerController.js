({
    closeModal : function(component, event, helper) {
		//component.set("v.inputValue",event.getParam('value'));
        var modal = event.getParam('value');
        if(modal){
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
        }
	},
    callSignatureComponent :function(component, event, helper) {
       
        component.set("v.showSignCmp",true);
        // var dismissActionPanel = $A.get("e.force:closeQuickAction");
        // dismissActionPanel.fire();
        
    },
    closeSignatureModal :function(component, event, helper) {
       
        component.set("v.showSignCmp",false);
        component.find("assessment").handleSignHere();

    },
    handleApplicationEvent :function(component, event, helper) {
       
        component.set("v.showSignCmp",false);
        //$A.get('e.force:refreshView').fire();
        component.find("assessment").handleRefresh();
        component.find("assessment").handleSignHere();


    }
})