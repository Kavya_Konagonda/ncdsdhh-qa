({
    initHandler : function(component, event, helper) {
        var action = component.get("c.getEquipmentDetails");
            action.setParams({
                requestId: component.get("v.recordId")
            });
        
            action.setCallback(this, function (response) {
            var result;
            result = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                //var dismissActionPanel = $A.get("e.force:closeQuickAction");
                var toastEvent = $A.get("e.force:showToast");
                //dismissActionPanel.fire();
                if(result == true) {
                    component.set("v.callLWC",true);
                } 
                else{
                    component.set("v.callLWC",false);
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": 'Add an Equipment to proceed before starting the Applicants Signature'
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                }
               
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();

                    console.log("Error message: " + JSON.stringify(errors));
                } else {
                    console.log("Unknown error");
                }
            }
        })
        $A.enqueueAction(action);
        // setTimeout(() => {
        //     var dismissActionPanel = $A.get("e.force:closeQuickAction");
        //     dismissActionPanel.fire();
        // }, 100)
    },
    closeModal : function(component, event, helper) {

        var modal = event.getParam('value');
        if(modal){
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
        }
	},

    
    callSignatureComponent :function(component, event, helper) {
       
        component.set("v.showSignCmp",true);
        // var dismissActionPanel = $A.get("e.force:closeQuickAction");
        // dismissActionPanel.fire();
        
    },

    closeSignatureModal :function(component, event, helper) {
       
        component.set("v.showSignCmp",false);
        component.find("applicantsSignature").handleSignHere();

    },
    handleApplicationEvent :function(component, event, helper) {
       
        component.set("v.showSignCmp",false);
        //$A.get('e.force:refreshView').fire();
        component.find("applicantsSignature").handleRefresh();
        component.find("applicantsSignature").handleSignHere();


    }
   
})