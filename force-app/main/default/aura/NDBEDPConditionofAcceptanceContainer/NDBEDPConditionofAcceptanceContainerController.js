({
    closeModal : function(component, event, helper) {
        var modal = event.getParam('value');
        if(modal){
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
        }
	},
    callSignatureComponent :function(component, event, helper) {
       
        component.set("v.showSignCmp",true);
        // var dismissActionPanel = $A.get("e.force:closeQuickAction");
        // dismissActionPanel.fire();
        
    },
    closeSignatureModal :function(component, event, helper) {
       
        component.set("v.showSignCmp",false);
        component.find("conditionOfAcceptance").handleSignHere();

    },
    handleApplicationEvent :function(component, event, helper) {
       
        component.set("v.showSignCmp",false);
        //$A.get('e.force:refreshView').fire();
        component.find("conditionOfAcceptance").handleRefresh();
        component.find("conditionOfAcceptance").handleSignHere();

    },
})