/**
 * @description       : 
 * @author            : Gourav Nirwal
 * @group             : 
 * @last modified on  : 06-08-2021
 * @last modified by  : Gourav Nirwal
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   06-08-2021   Gourav Nirwal   Initial Version
**/
public class ApexScanForSecurityReportController {
    public static Map<Id,List<String>> classWithProfileAccess{get;set;}
    public List<classWrapper> classlist{get;set;}
    public static Map<Id,SecurityScannerWrapper.SymbolTable> classMapWithSymbolTable{get;set;}
    
    public ApexScanForSecurityReportController(){
        try{
            classWithProfileAccess=  getSetupEntityAccesses();
            classMapWithSymbolTable = new Map<Id,SecurityScannerWrapper.SymbolTable>();
            String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
            String sessionId = UserInfo.getSessionId().substring(15);
            //getting Data from tooling api 
            SecurityScannerWrapper wrapperResponse= ApexScanForSecurityReportController.restGet( baseURL +
                                                                                            '/services/data/v47.0/tooling/query?'+
                                                                                            'q=Select+Id,Name,SymbolTable+From+ApexClass+where+NamespacePrefix+=\'\'+ORDER+BY+Name',
                                                                                            'GET', sessionId);
            System.debug(wrapperResponse);
            
            for(SecurityScannerWrapper.Record classRecord: wrapperResponse.Records){
                classMapWithSymbolTable.put(classRecord.id , classRecord.SymbolTable);
            }
            //Geting all the valid class list
            classlist= getClassList();
        } catch(Exception ex) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage() + '   ' + Ex.getStackTraceString()));
        }
    }
    
    public List<classWrapper> getClassList(){
        
        List<classWrapper> allClassWrapper = new List<classWrapper>(); 
        final List<String> SKIP_CLASSES = new List<String>{
            'OE_Scanner',
                'OE_Scanner_Test',
                'OE_Scanner_TestUtils',
                'OE_ScannerRecordGetter',
                'OE_BasicStub',
                'codeReviewController'
                };
                //Filter apex class and removing the test class and trigger handler from it
                    List<ApexClass> allClasses = [Select Id,Name,body From ApexClass Where NamespacePrefix = '' ORDER BY Name];
        
        for(ApexClass classRecord : allClasses ){
            allClassWrapper.add(new classWrapper(classRecord));
        }
        
        return allClassWrapper;
    }
    
    // public  void getClasses() {
    //     final List<String> SKIP_CLASSES = new List<String>{
    //         'OE_Scanner',
    //             'OE_Scanner_Test',
    //             'OE_Scanner_TestUtils',
    //             'OE_ScannerRecordGetter',
    //             'OE_BasicStub'
    //             };
    //                 List<ApexClass> allClasses = [Select Id,Name From ApexClass Where NamespacePrefix = '' AND (NOT NAME LIKE '%Test') AND (NOT NAME LIKE '%TriggerHandler') ORDER BY Name];
    //     List<ApexClass> result =  (List<ApexClass>) [
    //         FIND '@AuraEnabled'
    //         RETURNING
    //         ApexClass(Id, Name WHERE Name NOT IN :SKIP_CLASSES AND (NOT NAME LIKE '%Test')  AND (NOT NAME LIKE '%TriggerHandler') ORDER BY Name)
    //     ][0];
    //     String names= '';
    //     String Classnames= '';
    //     Set<Id> classIds= new Set<Id>();
    //     for(ApexClass ac : allClasses){
    //         classIds.add(ac.Id);
    //     }
    //     Map<Id, List<String>> accessIdsByClassIdByPermSetId = new Map<Id, List<String>>();

    // }
        /**
*————————————————————————————————————————————————————
* @Description
* Method is used to get all apex class and its access to which profile 
*————————————————————————————————————————————————————
**/
    public Map<Id,List<String>> getSetupEntityAccesses() {
        Map<Id,List<String>> mapClassesToBeReturned = new Map<Id,List<String>>();
        //getting information of 2 UserLicense -> Customer Community Plus Login and  Guest User License 
        for(SetupEntityAccess seAccess : [
            SELECT SetupEntityId, ParentId,Parent.Profile.Name  , Parent.Profile.UserLicense.name
            FROM SetupEntityAccess
            WHERE Parent.Profile.Name != null AND  (Parent.Profile.UserLicense.name = 'Customer Community Plus Login'
                                                    OR Parent.Profile.UserLicense.name = 'Guest User License')
        ]){
            
            if(!mapClassesToBeReturned.containsKey(seAccess.SetupEntityId )){
                List<String> profileList =  new List<String>();
                profileList.add(' ');
                profileList.add(' ');
                mapClassesToBeReturned.put(seAccess.SetupEntityId , profileList);
            }
            if(seAccess.Parent.Profile.UserLicense.name == 'Customer Community Plus Login'){
                mapClassesToBeReturned.get(seAccess.SetupEntityId )[0] += (seAccess.Parent.Profile.name + ', ');
            }
            else if(seAccess.Parent.Profile.UserLicense.name == 'Guest User License'){
                mapClassesToBeReturned.get(seAccess.SetupEntityId )[1] += (seAccess.Parent.Profile.name + ', ');
            }
            
        }
        //return map of (SetupEntityId and profile List)
        return mapClassesToBeReturned ;
    }
    public static SecurityScannerWrapper restGet(String endPoint, String method, String sid) {
        Http h = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setHeader('Authorization', 'Bearer ' + sid);
        hr.setTimeout(60000);
        hr.setEndpoint(endPoint);
        hr.setMethod(method);
        HttpResponse r = h.send(hr);
        return SecurityScannerWrapper.parse(r.getBody());
    }

    public class methodWrapper{
        
        public boolean isAuraEnabled {get;set;}
        public boolean isSOQLInjectionPossible{get;set;}
        public boolean isParameterUsedInSOQL{get;set;}
        public boolean isParameterUsedInDML{get;set;}
        public string name {get;set;}
        public list<SecurityScannerWrapper.Parameter> parameters{get;set;}
        public list<String> possibleIssues{get;set;}
        public string body {get;set;}
        public string dmlOperation{get;set;}
        
        
        public methodWrapper(SecurityScannerWrapper.method methodData,String uncommentedClassBody){
            this.name = methodData.name ;
            this.parameters = methodData.parameters ;
            String regex =name+'\\(.*?\\)\\s*\\{.*?\\}(?=\\s*(public|private|protected|static))';
            
            String regex2 = name+'\\(.*?\\)\\s*\\{.*?\\}(?=(\\s*}\\s*$))';
            
            regex = '(' + regex +')|('+ regex2 + '){1}?';
            
            system.debug('@@@@@@'+regex);
            system.debug('@@@@@@'+uncommentedClassBody);
            Matcher m = Pattern.compile(regex).matcher(uncommentedClassBody);
            //this.body = m.find() ? m.group() : uncommentedClassBody.replaceAll('(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)','');
        }
    }
    
    
    public class classWrapper{
        
        public string className{get;set;}
        public string runMode{get;set;}
        public Set<String> dmlOperation{get;set;}
        public boolean isHavingAuraEnabledMethod{get;set;}
        public boolean isCommunityUserAccessible{get;set;}
        public boolean isGuestUserAccessible{get;set;}
        public string communityUserProfile{get;set;}
        public string externalUserProfile{get;set;}
        public List<methodWrapper> methodList{get;set;}
        public string severity{get;set;}
        
        
        public classWrapper(ApexClass apexClassRecord){
            
            this.className = apexClassRecord.Name ; 
            string runModel1 = apexClassRecord.body.substringBetween('Public', 'Class');
            
            string runModel2 = apexClassRecord.body.containsIgnoreCase('With Sharing')? 'With Sharing':
            apexClassRecord.body.containsIgnoreCase('Without Sharing')? 'Without Sharing':
            apexClassRecord.body.containsIgnoreCase('Inherited Sharing')? 'Inherited Sharing':'Not Specified';
            
            dmlOperation = new Set<String>();
            dmlOperation.add(apexClassRecord.body.containsIgnoreCase('UPDATE ')? 'UPDATE' : apexClassRecord.body.containsIgnoreCase('Database.update(') ? 'UPDATE': null);
            dmlOperation.add(apexClassRecord.body.containsIgnoreCase('INSERT ')? 'INSERT' : apexClassRecord.body.containsIgnoreCase('Database.insert(') ? 'INSERT':null);
            dmlOperation.add(apexClassRecord.body.containsIgnoreCase('DELETE ')? 'DELETE' : apexClassRecord.body.containsIgnoreCase('Database.delete(') ? 'DELETE': null);
            dmlOperation.add(apexClassRecord.body.containsIgnoreCase('UPSERT ')? 'UPSERT' : apexClassRecord.body.containsIgnoreCase('Database.upsert(') ? 'UPSERT': null);
            if(!dmlOperation.isEmpty()){
                dmlOperation.remove(null);
            } 
            //this.dmlOperation = String.join(new List<String>(dmlOperation), '\n');
            this.runMode = runModel2 ;
            
            if(classWithProfileAccess.containsKey(apexClassRecord.id)){
                this.communityUserProfile = classWithProfileAccess.get(apexClassRecord.id)[0].removeEnd(', ');
                this.externalUserProfile = classWithProfileAccess.get(apexClassRecord.id)[1].removeEnd(', ');
                
                this.isCommunityUserAccessible= classWithProfileAccess.get(apexClassRecord.id)[0] != ' ' ;
                this.isGuestUserAccessible= classWithProfileAccess.get(apexClassRecord.id)[1] !=' ';
                
            }
            
            this.isHavingAuraEnabledMethod = apexClassRecord.body.containsIgnoreCase('@AuraEnabled');
            this.severity = 'Low';
            if(this.runMode == 'Without Sharing' && this.isHavingAuraEnabledMethod == true){
                if(this.isGuestUserAccessible== true) {
                    this.severity = 'High';
                }else{
                    this.severity = 'Medium';
                }
            }
            this.methodList= new List<methodWrapper>();
            for( SecurityScannerWrapper.Method methodRecord: classMapWithSymbolTable.get(apexClassRecord.id).methods) 
                this.methodList.add(new methodWrapper(methodRecord,apexClassRecord.body));
        }
        
    }
    
}