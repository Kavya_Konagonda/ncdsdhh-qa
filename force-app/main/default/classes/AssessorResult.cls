public without sharing class AssessorResult {
    @AuraEnabled
    public static Boolean assessorResult(String requestId,String assesserResult){
        String sObjName ='';
        if(String.isNotBlank(requestId)){
             sObjName = Id.valueOf(requestId).getSObjectType().getDescribe().getName();
        }
        if(sObjName == 'Case'){
        List<Case> caseFinalList = new List<Case>();
        for(Case caseValue : [SELECT Id,Assessor_Result__c,showInternalAssessor__c,CaseNumber FROM Case WHERE Id = :requestId WITH SECURITY_ENFORCED LIMIT 1]){
            caseValue.Assessor_Result__c = assesserResult;
            caseValue.showInternalAssessor__c=false;
            caseFinalList.add(caseValue);
        }
        System.debug('caseFinalList'+caseFinalList);
        if(caseFinalList.size()>0){
            SObjectAccessDecision objectDecision = SecurityLibrary.getAccessibleData('Case', caseFinalList, 'update', true,'AssessorResult','assessorResult');
            update objectDecision.getRecords();

            //update caseFinalList;
            return true;
        }
    }
    else{
        List<Attendees__c> AttendeesFinalList = new List<Attendees__c>();
        for(Attendees__c attendeesValue : [SELECT Id,Trainer_Result__c,showInternalAssessor__c,Name FROM Attendees__c WHERE Id = :requestId WITH SECURITY_ENFORCED LIMIT 1]){
            attendeesValue.Trainer_Result__c = assesserResult;
            attendeesValue.showInternalAssessor__c=false;
            AttendeesFinalList.add(attendeesValue);
        }
        System.debug('caseFinalList'+AttendeesFinalList);
        if(AttendeesFinalList.size()>0){
            SObjectAccessDecision objectDecision = SecurityLibrary.getAccessibleData('Attendees__c', AttendeesFinalList, 'update', true,'AssessorResult','assessorResult');
            update objectDecision.getRecords();
            //update AttendeesFinalList;
            return true;
        }
    }
        return false;
        
    }

    @AuraEnabled
    public static Boolean getAssessorResult(String requestId){
        String sObjName ='';
        if(String.isNotBlank(requestId)){
             sObjName = Id.valueOf(requestId).getSObjectType().getDescribe().getName();
        }
        Boolean assesserResult =false;
        if(sObjName == 'Case'){
        for(Case caseValue : [SELECT Id,Assessor_Result__c,CaseNumber FROM Case WHERE Id = :requestId WITH SECURITY_ENFORCED LIMIT 1]){
            System.debug('caseValue.Assessor_Result__c'+caseValue.Assessor_Result__c);
            if(caseValue.Assessor_Result__c==null){
                assesserResult= false;
            }
            else{
                assesserResult= true;
            }
        }
    }
    else{
        for(Attendees__c attendeesValue : [SELECT Id,Trainer_Result__c,showInternalAssessor__c,Name FROM Attendees__c WHERE Id = :requestId WITH SECURITY_ENFORCED LIMIT 1]){
           // System.debug('caseValue.Assessor_Result__c'+caseValue.Assessor_Result__c);
            if(attendeesValue.Trainer_Result__c==null){
                assesserResult= false;
            }
            else{
                assesserResult= true;
            }
        }
    }
        return assesserResult;
       
    }

    @AuraEnabled
    public static Boolean checkAssessor(String requestId){
        String sObjName ='';
        if(String.isNotBlank(requestId)){
             sObjName = Id.valueOf(requestId).getSObjectType().getDescribe().getName();
        }
        
        System.debug('requestId'+requestId);
        User user =[SELECT Id,IsPortalEnabled FROM User WHERE Id = :UserInfo.getUserId() WITH SECURITY_ENFORCED LIMIT 1];
        System.debug('user'+user);
        Case caseValue = new Case();
        Attendees__c attendee = new Attendees__c();
    if(sObjName  == 'Case'){
        caseValue=[SELECT Id,External_Assessor_User__c FROM Case WHERE Id =:requestId WITH SECURITY_ENFORCED LIMIT 1];
        if(caseValue.External_Assessor_User__c == user.Id){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        attendee=[SELECT Id,NDBEDP_Trainer_User__c FROM Attendees__c WHERE Id =:requestId WITH SECURITY_ENFORCED LIMIT 1];
        if(attendee.NDBEDP_Trainer_User__c == user.Id){
            return true;
        }
        else{
            return false;
        }
    }
        
        
        // System.debug('caseValue'+attendee.NDBEDP_Trainer_User__c == user.Id);
        // if(caseValue.External_Assessor_User__c == user.Id || attendee.NDBEDP_Trainer_User__c == user.Id){
        //     return true;
        // }
        // else{
        //     return false;
        // }

       
    }

    @AuraEnabled
    public static String getCaseNumber(String requestId){
        String sObjName ='';
        if(String.isNotBlank(requestId)){
             sObjName = Id.valueOf(requestId).getSObjectType().getDescribe().getName();
        }
        Case caseValue = new Case();
        Attendees__c attendee = new Attendees__c();
    if(sObjName  == 'Case'){
         caseValue = [SELECT Id,Assessor_Result__c,CaseNumber FROM Case WHERE Id = :requestId WITH SECURITY_ENFORCED LIMIT 1];
    }
    else{
        attendee= [SELECT Id, Name, Outreach_Request__r.Name, NDBEDP_Trainer__c, Trainer_Result__c, NDBEDP_Trainer_User__c FROM Attendees__c WHERE Id = :requestId WITH SECURITY_ENFORCED  LIMIT 1];
    }
    if(caseValue.CaseNumber!=null){
        return caseValue.CaseNumber;
        }
        else{
           return attendee.Outreach_Request__r.Name;
        }
        }
    

    @AuraEnabled
    public static List<Case> getCasesFromAccount(){
        User user =[SELECT Id, AccountId,IsPortalEnabled FROM User WHERE Id = :UserInfo.getUserId() WITH SECURITY_ENFORCED LIMIT 1];
        List<Case> caseList = new List<Case>();
        List<Case> trainerList = new List<Case>();
        if(user.IsPortalEnabled){
            trainerList=[SELECT Id,Assessor_Result__c,AccountId,CaseNumber,NDBEDP_Trainer__c,Status,ContactEmail,Contact.Name FROM Case WHERE NDBEDP_Trainer__c =:user.AccountId AND Assessor_Result__c=null WITH SECURITY_ENFORCED];
        }
        if(user.IsPortalEnabled){
                caseList =[SELECT Id,Assessor_Result__c,AccountId,CaseNumber,External_Assessor__c,Status,ContactEmail,Contact.Name FROM Case WHERE External_Assessor__c =:user.AccountId AND Assessor_Result__c=null WITH SECURITY_ENFORCED];
        }
        else{
            caseList=[SELECT Id,Assessor_Result__c,AccountId,CaseNumber,Internal_Assessor__c,Status,Contact.Name,ContactEmail FROM Case WHERE Internal_Assessor__c =:UserInfo.getUserId() AND Assessor_Result__c=null WITH SECURITY_ENFORCED];
        }
        if(trainerList.size()>0){
        caseList.addAll(trainerList);
        }

        return caseList;
    }
}