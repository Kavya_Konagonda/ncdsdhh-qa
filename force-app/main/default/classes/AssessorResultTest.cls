@isTest
public class AssessorResultTest {
    
    @TestSetup
    public static void makeData(){
        
        Id recordId = NC_Utility.RecordTypeUtilityMethod('Account','Assessor');
        Account acc=new Account();
        acc.RecordTypeId=recordId;
        acc.Email__c='test@gmail.com';
        acc.Name='Test';
        acc.BillingCity='city';
        acc.BillingCountry='United States';
        acc.BillingState='Alabama';
        acc.BillingPostalCode='12345';
        acc.BillingStreet='123 Street';
        acc.Active__c=true;
        insert acc;
        
        Contact con=new Contact();   
       // con.AccountId=acc.Id;
        con.LastName='Test Con';
        insert con;
        
       Case cas= new Case();
       cas.ContactId=con.Id;
       cas.External_Assessor__c=acc.Id;
       insert cas;
        
    }
    @isTest
    public static void testassessorResult(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
       // Boolean result=AssessorResult.assessorResult(cas.Id,'Accept');
        Test.stopTest();
    }
    
    @isTest
    public static void testgetAssessorResult(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        Boolean result=AssessorResult.getAssessorResult(cas.Id);
        System.assertEquals(false, result);
        Boolean result1=AssessorResult.getAssessorResult(null);
        System.assertEquals(false, result);
        Test.stopTest();
    }
    @isTest
    public static void testcheckAssessor(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        Boolean result=AssessorResult.checkAssessor(cas.Id);
        System.assertEquals(false, result);
        Test.stopTest();
    }
    @isTest
    public static void testgetCaseNumber(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        String result=AssessorResult.getCaseNumber(cas.Id);
        System.assertEquals('00103692', result);
        Test.stopTest();
    }
     @isTest
    public static void testGetCasesFromAccount(){
        Test.startTest();
        List<Case> resultList=AssessorResult.getCasesFromAccount();
        System.assertEquals(0, resultList.size());
        Test.stopTest();
    }

}