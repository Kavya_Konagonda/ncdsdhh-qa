/*
*   Class Name: NC_AccountTriggerTest
*   Description: 
*
*   Date            New/Modified         User                 Identifier                Description
*   01/05/2020         New         Hiten Aggarwal(mtx)
*/
@isTest
public class NC_AccountTriggerTest {
    
    // Metadata records of county and zip
    public static List<Regional_Center_And_County__mdt> regionalCenterCountyRecordList = [SELECT Id, Regional_Center__c,County__c, City__c, Zip_Code__c 
                                                                                          FROM Regional_Center_And_County__mdt
                                                                                          Limit 5];
    // Account Record Type 
    public static final Id vendorRecordTypeId =  NC_Utility.RecordTypeUtilityMethod('Account',  NC_Constant.Account_Vendor_RecordType);
    public static final Id regionalCenterRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Account', NC_Constant.Account_Regional_Center_RecordType);
    
    /*
    * Method Name: makeData
    * Description: Data Setup
    * @param:
    * @return void
    */
    @testSetup
    public static void makeData(){
       
       if(regionalCenterCountyRecordList.size() >0){
            List<Account> testAccountList = NC_AccountDataFactory.createAccountRecords(regionalCenterCountyRecordList[0].Regional_Center__c,1 ,false);
            Integer count = 0;
            for ( Account acccountRecord : testAccountList ) {
                acccountRecord.RecordTypeId = (count == 0) ? vendorRecordTypeId : regionalCenterRecordTypeId;
                acccountRecord.BillingPostalCode = regionalCenterCountyRecordList[0].Zip_Code__c;
                acccountRecord.BillingCity = regionalCenterCountyRecordList[0].City__c;        
                count++;
            }
          
           Insert testAccountList;
        }
        
    }
    
   /*
    * Method Name: beforeInsert_testNegative
    * Description:  test Insert - When no city and zipcode avaiable on account || When city or zip is not present in the meta data (Negative Test Case)
    * @param:
    * @return void
    */
    
    public static testMethod void beforeInsert_testNegative () {

        List<Account> testAccountList = NC_AccountDataFactory.createAccountRecords('accountName',2,false);
        Integer count = 0;
        for ( Account acccountRecord : testAccountList ) {
            acccountRecord.RecordTypeId = regionalCenterRecordTypeId;
            
            // Record with wrong city and zip combination
            if(count==0){
                acccountRecord.BillingPostalCode = '12012';
                acccountRecord.BillingCity = 'Invalid City';  
            }
            count++;
        }
        
        Test.startTest();
        Insert testAccountList;
        Test.stopTest();
    }

    /*
    * Method Name: beforeInsert_testPositive
    * Description:  test Insert - When city and zipcode avaiable on account is corrert but Name does not match with regional centre 
    *                             And  When city and zipcode avaiable on account is corrert but Name match with regional centre
    * @param:
    * @return void
    */
    
    public static testMethod void beforeInsert_testPositive () {
        // When city and zipcode avaiable on account is corrert but Name does not match with regional centre 
        List<Account> testAccountList = NC_AccountDataFactory.createAccountRecords('No Match With Any Regional Centre',2,false);
        for ( Account acccountRecord : testAccountList ) {
            acccountRecord.RecordTypeId = regionalCenterRecordTypeId;
            // Record with corret city and zip combination
            if(regionalCenterCountyRecordList.size() > 1){
                acccountRecord.BillingPostalCode = regionalCenterCountyRecordList[1].Zip_Code__c;
                acccountRecord.BillingCity = regionalCenterCountyRecordList[1].City__c;        
            }else{
                acccountRecord.BillingCity = 'Delco';         
                acccountRecord.BillingPostalCode = '28436';
            }
        }
      
        
        Insert testAccountList;
        
        
        // When city and zipcode avaiable on account is corrert but Name match with regional centre
        if(regionalCenterCountyRecordList.size() > 1){
            List<Account> accountList = NC_AccountDataFactory.createAccountRecords(regionalCenterCountyRecordList[0].Regional_Center__c,2,false);
            for ( Account acccountRecord : accountList ) {
                acccountRecord.RecordTypeId = regionalCenterRecordTypeId; 
                // Record with corret city and zip combination
                acccountRecord.BillingPostalCode = regionalCenterCountyRecordList[1].Zip_Code__c;
                acccountRecord.BillingCity = regionalCenterCountyRecordList[1].City__c;        
                
            }
            Test.startTest();
            Insert accountList;
            Test.stopTest();
          
        }
    }
    
    
   /*
    * Method Name: beforeUpdate_test
    * Description: test update
    * @param:
    * @return void
    */
    
    public static testMethod void beforeUpdate_test () {
        
        List<Account> testAccountList = [SELECT Id, Name, BillingCity, BillingPostalCode 
                                         FROM  Account];
        for ( Account acccountRecord : testAccountList ) {
            
            if(regionalCenterCountyRecordList.size() > 2){
                acccountRecord.Name = regionalCenterCountyRecordList[2].Regional_Center__c;
                acccountRecord.BillingPostalCode = regionalCenterCountyRecordList[2].Zip_Code__c;
                acccountRecord.BillingCity = regionalCenterCountyRecordList[2].City__c;        
            }else{
                acccountRecord.Name = 'Greensboro Regional Center';
                acccountRecord.BillingCity = 'Greensboro';         
                acccountRecord.BillingPostalCode = '27402';
            }
        }
        Test.startTest();
        Update testAccountList;
        Test.stopTest();
       
    }


}