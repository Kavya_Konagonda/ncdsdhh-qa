@isTest
public class NC_ApplicantSignatureTest {
 @TestSetup
    public static void makeData(){
        
         Id recordId = NC_Utility.RecordTypeUtilityMethod('Account','Assessor');
        Account acc=new Account();
        acc.RecordTypeId=recordId;
        acc.Email__c='test@gmail.com';
        acc.Name='Test';
        acc.BillingCity='city';
        acc.BillingCountry='United States';
        acc.BillingState='Alabama';
        acc.BillingPostalCode='12345';
        acc.BillingStreet='123 Street';
        acc.Active__c=true;
        insert acc;

        
        Contact con=new Contact();   
       // con.AccountId=acc.Id;
        con.LastName='Test Con';
        insert con;

        
        
       Case cas= new Case();
       cas.ContactId=con.Id;
       cas.External_Assessor__c=acc.Id;
      // cas.Internal_Assessor__c='';
        cas.Assessor_Comment__c ='test';
         cas.Comments__c ='';
       insert cas;
        
      /*  Case cas1= new Case();
       cas1.ContactId=con.Id;
       cas1.Internal_Assessor__c=user.Id;
        cas1.Assessor_Comment__c ='test';
        cas1.Comments__c ='';
       insert cas1;*/
        
        Assessment__c ass =new Assessment__c();
        ass.Request__c =cas.Id;
        Insert ass;
        
        
    }
    @isTest
    public static void testGetEquipmentDetails(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        Boolean result=NC_ApplicantSignature.getEquipmentDetails(cas.Id);
        Test.stopTest();
    }
    @isTest
    public static void saveCommentDate(){
        Case cas =[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
            try{
                NC_ApplicantSignature.saveCommentData('{attestorsignature":"00Pr00000045lEOEAY","attestationdate":"2021-11-23","personCompletingInformation":"Screen reader"hearing","relationship":"test","phoneNumber":"467889977","alternatecontact":"test contact","relationshipofalternatecontact":"test","phoneNumberofalternatecontact":"1234567890","emailaddress":"test@gmail.com","signed":"00Pr00000045l5AEAQ",comments":"test",requestId":"'+cas.Id+'\"}');
                  }catch(Exception e){
            throw e;
       }
        Test.stopTest();
    }
   
     @isTest
    public static void getConsumerNameFromRequestTest(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        NC_ApplicantSignature.getConsumerNameFromRequest(cas.Id);
        Test.stopTest();
    }
    @isTest
    public static void testGetTodaysDate(){
        Test.startTest();
        Date result=NC_ApplicantSignature.getTodaysDate();
        Test.stopTest();
    }
   
      @isTest
    public static void fetchSignatureTest(){
        Case cas =[SELECT Id FROM Case LIMIT 1];
                Test.startTest();
            try{
                NC_ApplicantSignature.fetchSignature(cas.Id);
                NC_ApplicantSignature.fetchFields(cas.Id);
             
                }catch(Exception e){
            throw e;
       }
    }
    @isTest
    public static void testGetAccountFromRequest(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        Case result=NC_ApplicantSignature.getAccountFromRequest(cas.Id);
        Test.stopTest();
    }
    @isTest
    public static void testGetStatus(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        Map<String,Object> result=NC_ApplicantSignature.getStatus(cas.Id);
        Test.stopTest();
    }
    
   
        @isTest
    public static void testgetAssessorResult(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        NC_AssessmentCtrl.fetchPicklist('Case','Assessor_Result__c');
        Test.stopTest();
    }
           
           
     
    @isTest
    public static void saveDataTest(){
        Case cas=[SELECT Id,External_Assessor__c FROM Case LIMIT 1];
        Test.startTest();
        try{
                NC_ApplicantSignature.saveData('{attestorsignature":"00Pr00000045lEOEAY","attestationdate":"2021-11-23","personCompletingInformation":"Screen reader"hearing","relationship":"test","phoneNumber":"467889977","alternatecontact":"test contact","relationshipofalternatecontact":"test","phoneNumberofalternatecontact":"1234567890","emailaddress":"test@gmail.com","signed":"00Pr00000045l5AEAQ","fromaccept":"fromaccept","refreshData":false,requestId":"'+cas.Id+'\"}');
        }catch(Exception e){
            throw e;
       }
        Test.stopTest();
    }
    
    
}