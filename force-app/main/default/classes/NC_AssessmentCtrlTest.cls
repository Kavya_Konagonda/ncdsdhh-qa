@isTest
public class NC_AssessmentCtrlTest {

      @TestSetup
    public static void makeData(){
        
        
        Id recordId = NC_Utility.RecordTypeUtilityMethod('Account','Assessor');
        Account acc=new Account();
        acc.RecordTypeId=recordId;
        acc.Email__c='test@gmail.com';
        acc.Name='Test';
        acc.BillingCity='city';
        acc.BillingCountry='United States';
        acc.BillingState='Alabama';
        acc.BillingPostalCode='12345';
        acc.BillingStreet='123 Street';
        acc.Active__c=true;
        insert acc;

        
        Contact con=new Contact();   
       // con.AccountId=acc.Id;
        con.LastName='Test Con';
        insert con;

        
        
       Case cas= new Case();
       cas.ContactId=con.Id;
       cas.External_Assessor__c=acc.Id;
      // cas.Internal_Assessor__c='';
        cas.Assessor_Comment__c ='test';
         cas.Comments__c ='';
       insert cas;
        
      /*  Case cas1= new Case();
       cas1.ContactId=con.Id;
       cas1.Internal_Assessor__c=user.Id;
        cas1.Assessor_Comment__c ='test';
        cas1.Comments__c ='';
       insert cas1;*/
        
        Assessment__c ass =new Assessment__c();
        ass.Request__c =cas.Id;
        Insert ass;
        
        
    }
    
        @isTest
    public static void testgetAssessorResult(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        NC_AssessmentCtrl.fetchPicklist('Case','Assessor_Result__c');
        Test.stopTest();
    }
            @isTest
    public static void getConsumerNameFromRequestTest(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        NC_AssessmentCtrl.getConsumerNameFromRequest(cas.Id);
        Test.stopTest();
    }
                @isTest
    public static void getAccountFromRequestTest(){
        Case cas=[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
        NC_AssessmentCtrl.getAccountFromRequest(cas.Id);
        NC_AssessmentCtrl.getTodaysDate();
        Test.stopTest();
    }
    
      /*  public static void getAccountFromRequestTest1(){
            ID ProfileID = [ Select id,UserType from Profile where name = 'Specialist'].id;
        ID RoleId = [SELECT Id,Name FROM UserRole where name = 'Deaf-Blind Services Specialist'].Id;
        User user = new User(alias = 'test123', email='test123@noemail.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid= ProfileID, country='United States',IsActive =true,UserRoleId  = RoleId,
        timezonesidkey='America/Los_Angeles', username='testerTemp1@noemail.com'
       );
        insert user;
            system.debug('user'+user);
        Case cas=[SELECT Id,External_Assessor__c,Internal_Assessor__c FROM Case LIMIT 1];
            cas.External_Assessor__c = '';
            cas.Internal_Assessor__c =user.Id;
            update cas;
        Test.startTest();
        NC_AssessmentCtrl.getAccountFromRequest(cas.Id);
        Test.stopTest();
    }*/
    
                    @isTest
    public static void saveDataTest(){
        Case cas=[SELECT Id,External_Assessor__c FROM Case LIMIT 1];
        Test.startTest();
        try{
        NC_AssessmentCtrl.saveData('{"attestation":"test","attestationdate":"2021-11-23","attestorsignature":"","atUsage":["Screen reader"],"communicationskills":["Braille – non-contractual","Speech/verbal"],"computerusageexperience":["PC"],"connectivity":["Currently has cellular phone service"],"consumergoals":"test","hearing":"Hearing – Severe (80-90 db PTA in the better ear)","hearingassessment":["Can hear a loud ringer"],"levelofcomputerexperience":["Basic (PC)"],"mailingaddress":"887 Parkside Avenue//nTrenton//nCalifornia//n08618//nUnited States","narrative":"test","onlineAssessor":false,"other":["Speech – (alternative or augmentative communication)"],"phone":"467889977","signed":"00Pr00000045l5AEAQ","tactileassessment":["Uses tactile ASL"],"visionloss":"Visual – visual field restriction","visualassessment":["Can use a visual signaler"],"frombutton":"fromaccept","requestId":"'+cas.Id+'\","refreshData":false }');
        }catch(Exception e){
            throw e;
       }
        Test.stopTest();
    }
    
   @isTest
    public static void saveCommentDate(){
        Case cas =[SELECT Id FROM Case LIMIT 1];
        Test.startTest();
            try{
                NC_AssessmentCtrl.saveCommentData('{"attestation":"test","attestationdate":"2021-11-23","attestorsignature":"00Pr00000045lEOEAY","atUsage":["Screen reader"],"communicationskills":["Braille – non-contractual","Speech/verbal"],"computerusageexperience":["PC"],"connectivity":["Currently has cellular phone service"],"consumergoals":"test","hearing":"Hearing – Severe (80-90 db PTA in the better ear)","hearingassessment":["Can hear a loud ringer"],"levelofcomputerexperience":["Basic (PC)"],"mailingaddress":"887 Parkside Avenue//nTrenton//nCalifornia//n08618//nUnited States","narrative":"test","onlineAssessor":false,"other":["Speech – (alternative or augmentative communication)"],"phone":"467889977","signed":"00Pr00000045l5AEAQ","tactileassessment":["Uses tactile ASL"],"visionloss":"Visual – visual field restriction","visualassessment":["Can use a visual signaler"],"comments":"test","requestId":"'+cas.Id+'\"}');
                  }catch(Exception e){
            throw e;
       }
        Test.stopTest();
    }
       @isTest
    public static void fetchSignatureTest(){
        Case cas =[SELECT Id FROM Case LIMIT 1];
                Test.startTest();
            try{
                NC_AssessmentCtrl.fetchSignature(cas.Id);
                NC_AssessmentCtrl.getStatus(cas.Id);
                NC_AssessmentCtrl.fetchFields(cas.Id);
                }catch(Exception e){
            throw e;
       }
    }
    
    
    
}