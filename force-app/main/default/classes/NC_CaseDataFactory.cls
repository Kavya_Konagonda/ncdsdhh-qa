/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 06-21-2021
 * @last modified by  : kavya.konagonda@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   06-10-2021   kavya.konagonda@mtxb2b.com   Initial Version
**/
public with sharing class NC_CaseDataFactory {
     public static List<Case> createCaseRecords( Integer count, Boolean isInsert){
        List<Case> caseList = new List<Case>();
        for(Integer i=0; i<count; i++){
            Case  caseRecord = new Case();
            caseList.add(caseRecord);
        }
        if(isInsert){
            SObjectAccessDecision objectDecision = SecurityLibrary.getAccessibleData('Case', caseList, 'insert', true,'NC_CaseDataFactory','createCaseRecords');
            insert objectDecision.getRecords();
        }
        return caseList;
    }
}