/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 06-10-2021
 * @last modified by  : kavya.konagonda@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   06-10-2021   kavya.konagonda@mtxb2b.com   Initial Version
**/
public without sharing class NC_CommunityLoginController {
    
    @AuraEnabled 
    public static ContactInfoWrapper logInUser(String jsonString) {
        ContactInfoWrapper infoWrapperInstance = new ContactInfoWrapper();
        ContactWrapper contactWrapperInstance = (ContactWrapper)JSON.deserialize(jsonString, ContactWrapper.Class);
        String userId;
        
        string communityUrl = '';
        if (contactWrapperInstance.password != null && (contactWrapperInstance.password).length() > 1) {
            communityUrl =  NC_Constant.AFTER_LOGIN_URL;
        }

        ApexPages.PageReference lgn = Site.login(contactWrapperInstance.userName, contactWrapperInstance.password, communityUrl);
        if(!Test.isRunningTest()) {
            infoWrapperInstance.redirectUrl = lgn.getUrl();
            aura.redirect(lgn);
        }
        return infoWrapperInstance;
    }
    
    @AuraEnabled
    public static String forgotPassword(String jsonString) {
        ContactInfoWrapper infoWrapperInstance = new ContactInfoWrapper();
        ContactWrapper contactWrapperInstance = (ContactWrapper)JSON.deserialize(jsonString, ContactWrapper.Class);
        String userId;

        String url = './CheckPasswordResetEmail';
        try {
            if(!Site.isValidUsername(contactWrapperInstance.userName)) {
                return Label.Site.invalid_email;
            }
            Site.forgotPassword(contactWrapperInstance.userName);
            ApexPages.PageReference checkEmailRef = new PageReference(url);
            if(!Test.isRunningTest()){
                aura.redirect(checkEmailRef);
            }
            return 'success';
        }
        catch (Exception ex) {
            return ex.getMessage();
        }
        
    }
            
    public class ContactWrapper {
        @AuraEnabled public String ssn {get; set;}
        @AuraEnabled public Date dob {get; set;}
        @AuraEnabled public String lastName {get; set;}
        @AuraEnabled public String middleName {get; set;}
        @AuraEnabled public String firstName {get; set;}
        @AuraEnabled public String email {get; set;}
        @AuraEnabled public String userName {get; set;}
        @AuraEnabled public String password {get; set;}
        @AuraEnabled public String confirmPassword {get; set;}
    }

    public class ContactInfoWrapper {
        @AuraEnabled public Boolean isContactExist {get; set;}
        @AuraEnabled public Boolean isUserExist {get; set;}
        @AuraEnabled public String redirectUrl {get; set;}
        @AuraEnabled public String error {get; set;}
        @AuraEnabled public Contact contactRecord {get; set;}

        public ContactInfoWrapper() {
            this.isContactExist = false;
            this.isUserExist = false;
            this.redirectUrl = null;
            this.error = null;
            this.contactRecord = new Contact();
        }
    }
}