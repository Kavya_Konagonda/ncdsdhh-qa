@isTest
public class NC_ConnectEquipmentControllerTest {

    /*
    static testmethod void getRecordTypesTest() {
        String caseId = TestDataUtility.createNDBEDPCase(true).id;
        Map<String, String> resultMap = NC_ConnectEquipmentController.getRecordTypes(caseId);
        System.assertEquals('true', resultMap.get('isNDBEDPCase'));
        System.assertEquals(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NDBEDP').getRecordTypeId()+'', resultMap.get('CaseNDBEDPId'));
    }*/
    
    static testmethod void getEquipmentCategoriesTest(){
        case caseObj1 = TestDataUtility.createNDBEDPCase(true);
        Map<String, Object> resultMap1 = NC_ConnectEquipmentController.getEquipmentCategories(caseObj1.id);
        System.assertNotEquals(null, resultMap1.get('equipmentDetails'));
        
        case caseObj2 = TestDataUtility.createNDBEDPCase(false, caseObj1.ContactId);
        caseObj2.status = 'Closed';
        insert caseObj2;
        Map<String, Object> resultMap2 = NC_ConnectEquipmentController.getEquipmentCategories(caseObj2.id);
        System.assertEquals('New Equipment cannot be added as Request status is Closed', resultMap2.get('errorMessage'));
        
        case caseObj3 = TestDataUtility.createNDBEDPCase(false, caseObj1.ContactId);
        caseObj3.status = 'Approved';
        caseObj3.MatRequestObj__c = null;
        caseObj3.InActive_Equipments__c = false;
        insert caseObj3;
        Map<String, Object> resultMap3 = NC_ConnectEquipmentController.getEquipmentCategories(caseObj3.id);
        System.assertEquals('MatRequestObj should not be empty', resultMap3.get('errorMessage'));
        
        case caseObj4 = TestDataUtility.createNDBEDPCase(false, caseObj1.ContactId);
        caseObj4.status = 'Approved';
        caseObj4.MatRequestObj__c = 'Test';
        caseObj4.MatReqStatus__c = 'Invoiced';
        insert caseObj4;
        Map<String, Object> resultMap4 = NC_ConnectEquipmentController.getEquipmentCategories(caseObj4.id);
        System.assertEquals('New Equipment cannot be added as MatRequest status is Invoiced', resultMap4.get('errorMessage'));
        
        case caseObj5 = TestDataUtility.createNDBEDPCase(false, caseObj1.ContactId);
        caseObj5.status = 'Approved';
        caseObj5.MatRequestObj__c = 'Test';
        caseObj5.MatRequestID__c = 'Test';
        insert caseObj5;
        Map<String, Object> resultMap5 = NC_ConnectEquipmentController.getEquipmentCategories(caseObj5.id);
        System.assertEquals('New Equipment cannot be added as MatRequest status is Invoiced', resultMap5.get('errorMessage'));
        
        case caseObj6 = TestDataUtility.createNDBEDPCase(false, caseObj1.ContactId);
        caseObj6.status = 'Approved';
        caseObj6.MatRequestObj__c = 'Test';
        //caseObj6.MatRequestID__c = 'Test';
        insert caseObj6;
        Map<String, Object> resultMap6 = NC_ConnectEquipmentController.getEquipmentCategories(caseObj6.id);
        System.assertEquals('Cannot add new Equipment as MatReqStatus is cancelled', resultMap5.get('errorMessage'));
    }
    
    /*
    static testmethod void createEquipmentAndEquipmentRequestTest(){
        String caseId = TestDataUtility.createNDBEDPCase(true).id;
        String resultStr = NC_ConnectEquipmentController.createEquipmentAndEquipmentRequest('', caseId);
    }*/
}