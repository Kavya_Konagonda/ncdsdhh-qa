/*
*   Class Name: NC_ContactTriggerTest
*   Description: 
*
*     Date            New/Modified         User                 Identifier                Description
*   01/05/2020           New         Hiten Aggarwal(mtx)
*/
@isTest
public class NC_ContactTriggerTest {
    
    // Metadata records of county and zip
    public static List<Regional_Center_And_County__mdt> regionalCenterCountyRecordList = [SELECT Id, Regional_Center__c,County__c, City__c, Zip_Code__c 
                                                                                          FROM Regional_Center_And_County__mdt
                                                                                          Limit 5];
    // Account Record Type - Regional Centre
    public static final Id regionalCenterRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Account', NC_Constant.Account_Regional_Center_RecordType);
    
    /*
    * Method Name: makeData
    * Description: Data Setup
    * @param:
    * @return void
    */
    @testSetup
    public static void makeData(){
        
        if(regionalCenterCountyRecordList.size() >0){
            List<Account> testAccountList = NC_AccountDataFactory.createAccountRecords(regionalCenterCountyRecordList[0].Regional_Center__c,1 ,false);
            for ( Account acccountRecord : testAccountList ) {
                acccountRecord.RecordTypeId = regionalCenterRecordTypeId;
                acccountRecord.BillingPostalCode = regionalCenterCountyRecordList[0].Zip_Code__c;
                acccountRecord.BillingCity = regionalCenterCountyRecordList[0].City__c;        
            }
            Insert testAccountList;
            List<Contact> testContactList = NC_ContactDataFactory.createContactRecords('contactName',1,false);
            for ( Contact contactRecord : testContactList ) {
                contactRecord.OtherPostalCode = regionalCenterCountyRecordList[0].Zip_Code__c;
                contactRecord.OtherCity = regionalCenterCountyRecordList[0].City__c;
            }
            Insert testContactList;
        }
    }
   /*
    * Method Name: beforeInsert_testNegative
    * Description:  test Insert - When no city and zipcode avaiable on account || When city or zip is not present in the meta data (Negative Test Case)
    * @param:
    * @return void
    */
    
    public static testMethod void beforeInsert_testNegative () {
        
        List<Account> testAccountList = NC_AccountDataFactory.createAccountRecords('accountName',2,false);
        Integer count = 0;
        for ( Account acccountRecord : testAccountList ) {
            acccountRecord.RecordTypeId = regionalCenterRecordTypeId;
            
            // Record with wrong city and zip combination
            if(count==0){
                acccountRecord.BillingPostalCode = '12012';
                acccountRecord.BillingCity = 'Invalid City';  
            }
            count++;
        }
        
        Insert testAccountList;
        
        List<Contact> testContactList = NC_ContactDataFactory.createContactRecords('contactName',1,false);
        for ( Contact contactRecord : testContactList ) {
            contactRecord.AccountId = testAccountList[0].Id;
            contactRecord.OtherPostalCode = '12012';
            contactRecord.OtherCity = 'Invalid City';
        }
        Test.startTest();
        Insert testContactList;
        Test.stopTest();
    }
     /*
    * Method Name: beforeInsert_testPositive
    * Description:  test Insert - When city and zipcode avaiable on contact is corrert but Name match with regional centre    
    *                             
    * @param:
    * @return void
    */
    
    public static testMethod void beforeInsert_testPositive () {
        Account accountRecord = [SELECT Id FROM Account Limit 1];
        // When city and zipcode avaiable on contact is corrert but Name match with regional centre    
        List<Contact> testContactList = NC_ContactDataFactory.createContactRecords('contactName',1,false);
        for ( Contact contactRecord : testContactList ) {
            if(regionalCenterCountyRecordList.size() > 1){
                contactRecord.AccountId = accountRecord.Id;
                contactRecord.OtherPostalCode =  regionalCenterCountyRecordList[1].Zip_Code__c;
                contactRecord.OtherCity = regionalCenterCountyRecordList[1].City__c; 
            }else{
                contactRecord.OtherPostalCode = '28436';
                contactRecord.OtherCity = 'Delco';
            }
        }
        Test.startTest();
        Insert testContactList;
        Test.stopTest();
    }
    
    
   /*
    * Method Name: beforeUpdate_test
    * Description: test update
    * @param:
    * @return void
    */
    
    public static testMethod void beforeUpdate_test () {
        List<Contact> contactToBeUpdated = new List<Contact>();
        List<Contact> testContactList = [SELECT Id, Name, OtherCity , OtherPostalCode  
                                         FROM Contact];
        for ( Contact contactRecord : testContactList ) {
            
            if(regionalCenterCountyRecordList.size() > 2){
                contactRecord.OtherCity = regionalCenterCountyRecordList[2].Zip_Code__c;
                contactRecord.OtherPostalCode = regionalCenterCountyRecordList[2].City__c;        
            }else{
                contactRecord.OtherCity = 'Greensboro';         
                contactRecord.OtherPostalCode = '27402';
            }
            contactToBeUpdated.add(contactRecord);
        }
        Test.startTest();
        Update contactToBeUpdated;
        Test.stopTest();
        
    }

}