public with sharing class NC_ContentDocumentLinkTriggerHandler {
    public static void afterInsert(List<ContentDocumentLink> contentDocumentList, Map<Id, ContentDocumentLink> oldMap){
       // getAttachmentIdAfterInsert(contentDocumentList,oldMap);
    }
    public static void beforeInsert(List<ContentDocumentLink> contentDocumentList, Map<Id, ContentDocumentLink> oldMap){
        getAttachmentIdAfterInsert(contentDocumentList,oldMap);
    }
    public static void beforeUpdate(List<ContentDocumentLink> contentDocumentList, Map<Id, ContentDocumentLink> oldMap){
        
    }
    public static void afterUpdate(List<ContentDocumentLink> contentDocumentList, Map<Id, ContentDocumentLink> oldMap){

        
    }
    public static void afterDelete(Map<Id, ContentDocumentLink> oldMap){

        
    }
    public static void getAttachmentIdAfterInsert(List<ContentDocumentLink> contentDocumentList, Map<Id, ContentDocumentLink> oldMap){
        List <Domain> domains = [SELECT Domain, DomainType FROM Domain WHERE DomainType = 'DNS'];
        Id communicationAccessRequestRecordId = Schema.SObjectType.Communication_Access_Request__c.getRecordTypeInfosByDeveloperName().get('Generic_Request').getRecordTypeId();
        Map<Id,Id> recordIdToFileIdSet = new Map<Id,Id>();
        List<ContentDocumentLink> toUpdate = new List<ContentDocumentLink>();
        Set<Id> communicationRequestId = new Set<Id>();
        for(ContentDocumentLink att:contentDocumentList){
            if(att.LinkedEntityId.getSObjectType().getDescribe().getName() == 'Communication_Access_Request__c'){
             att.Visibility = 'AllUsers';
            
             communicationRequestId.add(att.LinkedEntityId);  
             recordIdToFileIdSet.put(att.LinkedEntityId, att.ContentDocumentId );
             }
         }

     
            
        System.debug('recordIdToFileIdSet'+recordIdToFileIdSet);
        List<Communication_Access_Request__c > communicationAccessList =new List<Communication_Access_Request__c>();

        for(Communication_Access_Request__c com:[SELECT Id,File_Record_Id_s__c,Files_Uploaded__c FROM Communication_Access_Request__c WHERE Id IN:communicationRequestId]){//WITH SECURITY_ENFORCED
         if(com.File_Record_Id_s__c!=null){
         com.File_Record_Id_s__c = com.File_Record_Id_s__c +'\n'+(URL.getSalesforceBaseUrl().toExternalForm()+'/'+recordIdToFileIdSet.get(com.Id));
         com.Files_Uploaded__c = com.Files_Uploaded__c + '\n' +('https://'+domains[0].Domain +'/'+recordIdToFileIdSet.get(com.Id));
         }
         else{
             com.File_Record_Id_s__c = URL.getSalesforceBaseUrl().toExternalForm()+'/'+recordIdToFileIdSet.get(com.Id);
             com.Files_Uploaded__c = 'https://'+domains[0].Domain +'/'+recordIdToFileIdSet.get(com.Id);
            // com.File_Record_Id_s__c = URL.getSalesforceBaseUrl().toExternalForm()+'/'+recordIdToFileIdSet.get(com.Id);
         }
         communicationAccessList.add(com);
         System.debug('com'+com);
        }
        if(communicationAccessList.size()>0){
            SObjectAccessDecision objectDecision = SecurityLibrary.getAccessibleData('Communication_Access_Request__c', communicationAccessList, 'update', true,'NC_ContentDocumentLinkTriggerHandler','getAttachmentIdAfterInsert');
            update objectDecision.getRecords();
           // update communicationAccessList;
        }
        // if(toUpdate.size()>0){
        //     update toUpdate;
        //  }  
    }

   
}