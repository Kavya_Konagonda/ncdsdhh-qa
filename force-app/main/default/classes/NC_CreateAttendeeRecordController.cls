public with sharing class NC_CreateAttendeeRecordController {
    @AuraEnabled
    public static Map<String,Object> retrieveDefaults(Id recordId){
        Map<String,Object> result = new Map<String,Object>();
        String objectName = recordId.getSobjectType().getDescribe().getName();
        if(objectName == 'Outreach_Request__c') {
            Id ndbedpEventTypeId = NC_Utility.RecordTypeUtilityMethod('Outreach_Request__c','NDBEDP_Event');

            List<Outreach_Request__c> outReachList = [SELECT recordTypeId FROM Outreach_Request__c WHERE id =: recordId WITH SECURITY_ENFORCED];
            if(!outReachList.isEmpty() && outReachList[0].recordTypeId == ndbedpEventTypeId) {
                result.put('recordTypeId',NC_Utility.RecordTypeUtilityMethod('Attendees__c','Attendees_For_NDBEDP_Event'));
            } else {
                result.put('recordTypeId',NC_Utility.RecordTypeUtilityMethod('Attendees__c','Attendees_For_Outreach_Request'));
            }
            result.put('forOutreach',true);
        }else{
            result.put('recordTypeId',NC_Utility.RecordTypeUtilityMethod('Attendees__c','Attendees_For_Event_Attribute'));
            result.put('forOutreach',false);
        }
        return result;
    }
}
/*
 public class NC_CreateAttendeeRecordController {
 @AuraEnabled
    public static Map<String,Object> retrieveDefaults(Id recordId){
        Map<String,Object> result = new Map<String,Object>();
        String objectName = recordId.getSobjectType().getDescribe().getName();
        if(objectName == 'Outreach_Request__c') {
            result.put('recordTypeId',NC_Utility.RecordTypeUtilityMethod('Attendees__c','Attendees_For_Outreach_Request'));
            result.put('forOutreach',true);
        }else{
            result.put('recordTypeId',NC_Utility.RecordTypeUtilityMethod('Attendees__c','Attendees_For_Event_Attribute'));
            result.put('forOutreach',false);
        }
        return result;
    }
}*/