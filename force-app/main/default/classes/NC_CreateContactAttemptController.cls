public class NC_CreateContactAttemptController {
	@AuraEnabled
    public static Map<String,Object> retrieveDefaults(Id recordId){
        Map<String,Object> result = new Map<String,Object>();
        String objectName = recordId.getSobjectType().getDescribe().getName();
        if(objectName == 'Asset__c') {
            result.put('recordTypeId',NC_Utility.RecordTypeUtilityMethod('Contact_Attempt__c','Contact_Attempt_For_Asset'));
            result.put('forAsset',true);
        }else if(objectName == 'Case'){
            result.put('recordTypeId',NC_Utility.RecordTypeUtilityMethod('Contact_Attempt__c','Contact_Attempt_For_Request'));
            result.put('forAsset',false);
        }
        return result;
    }
}