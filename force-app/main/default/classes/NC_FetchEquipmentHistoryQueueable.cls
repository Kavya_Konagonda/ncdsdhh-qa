public with sharing class NC_FetchEquipmentHistoryQueueable implements Queueable,Database.AllowsCallouts{
    public Contact contactObj;
    public NC_FetchEquipmentHistoryQueueable(Contact contactData) {
        contactObj = contactData;
    }

    public void execute(QueueableContext qc){
        NC_FetchEquipmentHistory.fetchEquipmentHistory(contactObj.id);
    }
}