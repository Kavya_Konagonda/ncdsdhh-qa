public with sharing class NC_IncomeWorksheetController {
    
    @AuraEnabled
    public static Map<String,Object> fetchContactData(Id recordId){
        //recordId is Of Request
        Boolean showErroMessage = false;
        Boolean alreadyFilled = false;
        Map<String, Object> result = new Map<String, Object>();
        Set<String> docSet = new Set<String>{'Work First','Medicaid','SSI',
            'CSHS (Children\'s’ Special Health Services)','Health Choice for Children',
            'Section 8 Housing Choice Vouchers','Food Stamps'}; 
        Id ndbedpRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Case','NDBEDP');
        Case caseRecord = [SELECT id, RecordTypeId, contactId,Status, Proof_of_Income_Provided_By__c, Equipment_Types__c FROM Case WHERE Id =: recordId WITH SECURITY_ENFORCED];
        Contact contactRecord = [SELECT id, Marital_Status__c, Proof_of_Income_Provided_By__c FROM Contact WHERE Id =: caseRecord.contactId WITH SECURITY_ENFORCED];
        List<Family_Details__c> familyDetailsList = [SELECT Id, Contact__c, Request__c FROM Family_Details__c WHERE Contact__c =:  caseRecord.contactId WITH SECURITY_ENFORCED];
        List<FamilyDetailsWrapper> FamilyDetailsWrapperList = new  List<FamilyDetailsWrapper>();
        String maritalStatus = contactRecord.Marital_Status__c != NULL ? contactRecord.Marital_Status__c : '-None-';
        String proofOfIncomeProvidedBy = contactRecord.Proof_of_Income_Provided_By__c != NULL ? contactRecord.Proof_of_Income_Provided_By__c : '-None-';
       // System.debug('caseRecord.Equipment_Types__c -'+ caseRecord.Equipment_Types__c);
        /*
        if(FamilyDetailsWrapperList.size()>0){
           alreadyFilled = true; 
        }else if((caseRecord.status != 'Submitted' && caseRecord.status != 'In Progress'
                  && caseRecord.status != 'Pending Documents' && caseRecord.status != 'Pending Condition of Acceptance') 
                  || caseRecord.Equipment_Types__c == 'Weather Radio'
                  || caseRecord.Proof_of_Income_Provided_By__c == 'Work First' || caseRecord.Proof_of_Income_Provided_By__c == 'Medicaid'  
                  || caseRecord.Proof_of_Income_Provided_By__c == 'SSI' || caseRecord.Proof_of_Income_Provided_By__c == 'CSHS (Children\'s’ Special Health Services)'
                  || caseRecord.Proof_of_Income_Provided_By__c == 'Health Choice for Children' || caseRecord.Proof_of_Income_Provided_By__c == 'Section 8 Housing Choice Vouchers'
                  || caseRecord.Proof_of_Income_Provided_By__c == 'Food Stamps' ){
           showErroMessage = true;
        }*/
        if(FamilyDetailsWrapperList.size()>0){
           alreadyFilled = true; 
        }else if(caseRecord.recordTypeId != ndbedpRecordTypeId && (caseRecord.status != 'Submitted' && caseRecord.status != 'In Progress'
                  && caseRecord.status != 'Pending Documents' && caseRecord.status != 'Pending Condition of Acceptance') 
                  || caseRecord.Equipment_Types__c == 'Weather Radio'
                  || docSet.contains(caseRecord.Proof_of_Income_Provided_By__c) ){
           showErroMessage = true;
        }else if(caseRecord.recordTypeId == ndbedpRecordTypeId && (caseRecord.status != 'Submitted' && caseRecord.status != 'In Progress'
        && caseRecord.status != 'Pending Documents' && caseRecord.status != 'Documents Review') 
                  || caseRecord.Proof_of_Income_Provided_By__c == null
                  || docSet.contains(caseRecord.Proof_of_Income_Provided_By__c)) {
            showErroMessage = true;
        }
        
        result.put('alreadyFilled',alreadyFilled);
        result.put('showErroMessage',showErroMessage);
        result.put('maritalStatus',maritalStatus);
        result.put('proofOfIncomeProvidedBy',proofOfIncomeProvidedBy);
        return result;
    }
    
    @AuraEnabled
    public static Map<String,Object> updateContactData(String familyJsonData, Id recordId){
        Map<String, Object> result = new Map<String, Object>();
        List<FamilyDetailsWrapper> FamilyDetails = (List<FamilyDetailsWrapper>) JSON.deserialize(familyJsonData, List<FamilyDetailsWrapper>.class);
        List<Family_Details__c> familyDetailsList = new List<Family_Details__c>(); 
        Decimal sumOfAllIncome = 0;
        Family_Details__c familyRecord ;
        String url = System.URL.getSalesforceBaseUrl().toExternalForm(); 
        Id ndbedpRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Case','NDBEDP');
        Case caseRecord = [SELECT Id,Status,ContactId, RecordTypeId FROM Case WHERE Id =: recordId WITH SECURITY_ENFORCED];
        
        for(FamilyDetailsWrapper family : FamilyDetails){
            familyRecord = new Family_Details__c();
            familyRecord.Name__c = family.name;
            familyRecord.Age__c = family.age;
            familyRecord.Relationship__c = family.relationship;
            familyRecord.Contact__c = caseRecord.ContactId;
            familyRecord.Request__c = caseRecord.Id;
            familyRecord.Alimony__c = family.incomeType.Alimony;
            familyRecord.AmeriCorps_Stipends__c = family.incomeType.AmeriCorps_Stipends;
            familyRecord.Annuities__c = family.incomeType.Annuities;
            familyRecord.Armed_Forces_pay__c = family.incomeType.Armed_Forces_pay;
            familyRecord.Cherokee_Tribal_per_captia_Income_paid_t__c = family.incomeType.Cherokee;
            familyRecord.Child_support__c = family.incomeType.Child_support;
            familyRecord.Gross_salary_and_wages__c = family.incomeType.Gross_Salary_and_Wages;
            familyRecord.On_the_Job_training__c = family.incomeType.On_the_Job_training;
            familyRecord.Rental_Income__c = family.incomeType.Rental_Income;
            familyRecord.Retirement_Pension__c = family.incomeType.Retirement_Pension;
            familyRecord.Social_Security__c= family.incomeType.Social_Security;
            familyRecord.Tobacco_buy_out_payments__c= family.incomeType.Tobacco_buy_out_payments;
            //familyRecord.Total_Income__c = family.incomeType.
            familyRecord.Unemployment__c= family.incomeType.Unemployment;
            familyRecord.Veteran_Administration_Benefits__c =family.incomeType.Veterans_Administration;
            familyRecord.Work_Release_Payments__c= family.incomeType.Work_Release_Payments;
            familyRecord.Worker_s_Compensation_payments__c = family.incomeType.Worker_Compensation_payments;
            sumOfAllIncome += family.totalIncome;
            familyDetailsList.add(familyRecord); 
        }
        Integer currenYear = Integer.valueOf(Date.Today().year());
        
        Map<Integer,Income_Worksheet__mdt> numberOfMemberToIncomeWorkSheet = new  Map<Integer,Income_Worksheet__mdt>();
        for(Income_Worksheet__mdt incomeWorkSheet : [SELECT Id, Number_of_Members__c, Amount__c, Year__c 
                                                     FROM Income_Worksheet__mdt 
                                                     WHERE Number_of_Members__c =: familyDetailsList.size() WITH SECURITY_ENFORCED
                                                     ORDER By Year__c ASC]){
                                                     //AND Year__c =: currenYear]){
               numberOfMemberToIncomeWorkSheet.put(Integer.valueOf(incomeWorkSheet.Number_of_Members__c), incomeWorkSheet);
        }
        //System.debug('familyDetailsList >'+ familyDetailsList.size());
        if(familyDetailsList.size()>0){
            SObjectAccessDecision decision =  SecurityLibrary.getAccessibleData('Case', familyDetailsList, 'insert', true, 'NC_IncomeWorksheetController','updateContactData');
            Insert decision.getRecords();
          // Insert familyDetailsList;
        }
       // System.debug('numberOfMemberToIncomeWorkSheet > '+ numberOfMemberToIncomeWorkSheet);
        Decimal sumOfAllIncomePercentage = sumOfAllIncome ;  //2.50 * sumOfAllIncome;
        if(ndbedpRecordTypeId != caseRecord.recordTypeId && numberOfMemberToIncomeWorkSheet.containskey(familyDetailsList.size()) &&  numberOfMemberToIncomeWorkSheet.get(familyDetailsList.size()).Amount__c*2.50 < sumOfAllIncomePercentage){ 
            if(caseRecord.Status != 'Denied'){
                caseRecord.Status = 'Denied';
               // Update caseRecord;
                SObjectAccessDecision decision =  SecurityLibrary.getAccessibleData('Case', new List<Case>{caseRecord}, 'update', true,'NC_IncomeWorksheetController','updateContactData');
                update decision.getRecords();
            }
        }

        //NDBEDP
        Map<Integer, NDBEDP_Income_Worksheet__mdt> memberToWorkSheetNDBEDP = new Map<Integer, NDBEDP_Income_Worksheet__mdt>();
        for(NDBEDP_Income_Worksheet__mdt metadata: NDBEDP_Income_Worksheet__mdt.getAll().values()) {
            memberToWorkSheetNDBEDP.put(Integer.valueOf(metadata.Number_of_Members__c), metadata);
        }
        if(ndbedpRecordTypeId == caseRecord.recordTypeId && memberToWorkSheetNDBEDP.containskey(familyDetailsList.size()) &&  memberToWorkSheetNDBEDP.get(familyDetailsList.size()).Amount__c*4 < sumOfAllIncomePercentage){ 
            if(caseRecord.Status != 'Denied'){
                caseRecord.Status = 'Denied';
                caseRecord.Income_more_than_400_percent__c = true;
                SObjectAccessDecision decision =  SecurityLibrary.getAccessibleData('Case', new List<Case>{caseRecord}, 'update', true,'NC_IncomeWorksheetController','updateContactData');
                update decision.getRecords();
            }
        }

       // System.debug('url -'+url);
        url = url+'/lightning/r/case/' + caseRecord.Id+'/view';
        result.put('url',url);

        result.put('success','Family Members Added');
        return result;
    }
    
    /*
    * Method Name: fetchPicklist
    * Description: Fetch Picklist Values
    * @param: String, String
    * @return List<SelectOptionWrapper>
    */
    
    @AuraEnabled
    public static List<SelectOptionWrapper> fetchPicklist(String objectName, String fieldName){
        List<SelectOptionWrapper> optionList = new List<SelectOptionWrapper>(); 
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        if(fields.containsKey(fieldName)){
            Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                optionList.add( new SelectOptionWrapper( pickListVal.getValue(), pickListVal.getLabel())  );
            }
        }
        
        return optionList;
    }


    public class IncomeWorkSheet{
        List<FamilyDetailsWrapper> listOfFamilyDetailsWrapper;

        IncomeWorkSheet(){
            listOfFamilyDetailsWrapper = new List<FamilyDetailsWrapper> ();
        }
    }


    public class incomeType{
        Decimal Alimony;
        Decimal AmeriCorps_Stipends;
        Decimal Annuities;
        Decimal Armed_Forces_pay;
        Decimal Cherokee;
        Decimal Child_support;
        Decimal Gross_Salary_and_Wages;
        Decimal On_the_Job_training;
        Decimal Rental_Income;
        Decimal Retirement_Pension;
        Decimal Social_Security;
        Decimal Tobacco_buy_out_payments;
        Decimal Unemployment;
        Decimal Veterans_Administration;
        Decimal Work_Release_Payments;
        Decimal Worker_Compensation_payments;

        public incomeType(){
            this.AmeriCorps_Stipends = 0.0;
            this.Annuities = 0.0;
            this.Armed_Forces_pay = 0.0;
            this.Cherokee = 0.0;
            this.Child_support = 0.0;
            this.Gross_Salary_and_Wages = 0.0;
            this.On_the_Job_training = 0.0;
            this.Rental_Income = 0.0;
            this.Retirement_Pension = 0.0;
            this.Social_Security = 0.0;
            this.Tobacco_buy_out_payments = 0.0;
            this.Unemployment = 0.0;
            this.Veterans_Administration = 0.0;
            this.Work_Release_Payments = 0.0;
            this.Alimony = 0.0;
            this.Worker_Compensation_payments = 0.0;   
        }
    }

    public class FamilyDetailsWrapper{
        Decimal totalIncome;
        Decimal age;
        String name;
        String relationship;
        incomeType incomeType;

        public FamilyDetailsWrapper(){
            this.age = Null;
            this.name = '';
            this.relationship = '';
            this.totalIncome = 0;
            this.incomeType = new incomeType();
        }
    }
    
    public class SelectOptionWrapper{
        @AuraEnabled public string value;
        @AuraEnabled public string label;
        
        public SelectOptionWrapper(string value, string label){
            this.value = value;
            this.label = label;
        }
    }

}