/*
**   Class Name: NC_InvoiceAuthorizationSharingBatch
**   Description: Batch class for sharing authorization and Invoice with Vendor portal users
**
**     Date            New/Modified           User                 Identifier                Description
**   26-05-2021             New          Ruchit Goswami
*/
public without sharing class NC_InvoiceAuthorizationSharingBatch implements Database.Batchable<sObject>,Schedulable {
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Vendor__c FROM Authorization__c WHERE Vendor__r.Is_Vendor_on_Portal__c = true AND CreatedBy.Profile.Name != \'Vendor Community User Profile\'';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Authorization__c> authList) {
        Map<String, Set<String>> userAuthMap = new Map<String, Set<String>>();
        Map<String, Set<String>> userInvoiceMap = new Map<String, Set<String>>();
        Map<String, Set<String>> vendorAuthMap = new Map<String, Set<String>>();
        
        for(Authorization__c authRecord: authList) {
            if (vendorAuthMap.containsKey(authRecord.Vendor__c)) {
                vendorAuthMap.get(authRecord.Vendor__c).add(authRecord.Id);
            } else {
                vendorAuthMap.put(authRecord.Vendor__c, new Set<String>{authRecord.Id});
            }
        }

        Set<String> vendorAuthIds = getAuthorizationIdSet(vendorAuthMap.values());
        Map<String, Set<String>> vendorInvoiceMap = new Map<String, Set<String>>();
        for(Invoice__c invoiceRecord: [SELECT Authorization__r.Vendor__r.Id FROM Invoice__c WHERE Authorization__c IN: vendorAuthIds AND CreatedBy.Profile.Name != 'Vendor Community User Profile']) {
            if(vendorInvoiceMap.containsKey(invoiceRecord.Authorization__r.Vendor__r.Id)) {
                vendorInvoiceMap.get(invoiceRecord.Authorization__r.Vendor__r.Id).add(invoiceRecord.Id);
            } else {
                vendorInvoiceMap.put(invoiceRecord.Authorization__r.Vendor__r.Id, new Set<String>{invoiceRecord.Id});
            }
        }
       // System.debug('vendorInvoiceMap-->'+vendorInvoiceMap);

        for(User userRecord: [SELECT AccountId FROM User WHERE IsActive = true AND Profile.Name = 'Vendor Community User Profile' AND AccountId IN: vendorAuthMap.keySet()]) {
            if(vendorAuthMap.containsKey(userRecord.AccountId)) {
                Set<String> authIds = vendorAuthMap.get(userRecord.AccountId);
                userAuthMap.put(userRecord.Id, authIds);
            }
            if(vendorInvoiceMap.containsKey(userRecord.AccountId)) {
                Set<String> invoiceIds = vendorInvoiceMap.get(userRecord.AccountId);
                userInvoiceMap.put(userRecord.Id, invoiceIds);
            }
        }

        shareAuthorizations(userAuthMap);
       // System.debug('userInvoiceMap-->'+userInvoiceMap);
        shareInvoices(userInvoiceMap);
    }

    public void shareAuthorizations(Map<String, Set<String>> userAuthMap) {
        List<Authorization__Share> authShareList = new List<Authorization__Share>();
        Set<String> authIds = getAuthorizationIdSet(userAuthMap.values());
        Map<String, Set<String>> authSharedMap = new Map<String, Set<String>>();

        for(Authorization__Share authShare: [SELECT ParentId, UserOrGroupId FROM Authorization__Share WHERE UserOrGroupId IN: userAuthMap.keySet() AND ParentId IN: authIds AND RowCause = 'Manual']) {
            if(authSharedMap.containsKey(authShare.UserOrGroupId)) {
                authSharedMap.get(authShare.UserOrGroupId).add(authShare.ParentId);
            } else {
                authSharedMap.put(authShare.UserOrGroupId, new Set<String>{authShare.ParentId});
            }
        }

        List<String> authIdList;
        for(String userId: userAuthMap.keySet()) {
            authIdList = new List<String>(userAuthMap.get(userId));
            if(authSharedMap.containsKey(userId)) {
                Set<String> authSharedList = authSharedMap.get(userId);
                for(String authId: authIdList) {
                    if(!authSharedList.contains(authId)){
                        addAuthShareRecord(authId, userId, authShareList);
                    }
                }
            } else {
                for(String authId: authIdList) {
                    addAuthShareRecord(authId, userId, authShareList);
                }
            }
        }

       // System.debug('authShareList-->'+authShareList.size());
        for(Authorization__Share authShare: authShareList) {
          //  System.debug('authShare-->'+authShare);
        }
        if(authShareList.size() > 0) {
            insert authShareList;
        }
    }

    public void shareInvoices(Map<String, Set<String>> userInvoiceMap) {
        List<Invoice__Share> invoiceShareList = new List<Invoice__Share>();
        Set<String> invIds = getAuthorizationIdSet(userInvoiceMap.values());
        Map<String, Set<String>> invAlreadySharedMap = new Map<String, Set<String>>();

        for(Invoice__Share invShare: [SELECT ParentId, UserOrGroupId FROM Invoice__Share WHERE UserOrGroupId IN: userInvoiceMap.keySet() AND ParentId IN: invIds AND RowCause = 'Manual']) {
            if(invAlreadySharedMap.containsKey(invShare.UserOrGroupId)) {
                invAlreadySharedMap.get(invShare.UserOrGroupId).add(invShare.ParentId);
            } else {
                invAlreadySharedMap.put(invShare.UserOrGroupId, new Set<String>{invShare.ParentId});
            }
        }
       // System.debug('invAlreadySharedMap-->'+invAlreadySharedMap);

        List<String> invIdList;
        for(String userId: userInvoiceMap.keySet()) {
            invIdList = new List<String>(userInvoiceMap.get(userId));
            if(invAlreadySharedMap.containsKey(userId)) {
                Set<String> invSharedList = invAlreadySharedMap.get(userId);
                for(String invId: invIdList) {
                    if(!invSharedList.contains(invId)){
                        addInvoiceShareRecord(invId, userId, invoiceShareList);
                    }
                }
            } else {
                for(String invId: invIdList) {
                    addInvoiceShareRecord(invId, userId, invoiceShareList);
                }
            }
        }

      //  System.debug('invoiceShareList-->'+invoiceShareList);
      //  System.debug('invoiceShareList-->'+invoiceShareList.size());
        if(invoiceShareList.size() > 0) {
            insert invoiceShareList;
        }
    }

    public void addAuthShareRecord(String parentId, String userOrGroupId, List<Authorization__Share> authShareList) {
        Authorization__Share authShare = new Authorization__Share();
        authShare.ParentId = parentId;
        authShare.UserOrGroupId = userOrGroupId;
        authShare.AccessLevel = 'Edit';
        authShare.RowCause = Schema.Authorization__Share.RowCause.Manual;
        authShareList.add(authShare);
    }

    public void addInvoiceShareRecord(String parentId, String userOrGroupId, List<Invoice__Share> invoiceShareList) {
        Invoice__Share inoviceShare = new Invoice__Share();
        inoviceShare.ParentId = parentId;
        inoviceShare.UserOrGroupId = userOrGroupId;
        inoviceShare.AccessLevel = 'Edit';
        inoviceShare.RowCause = Schema.Invoice__Share.RowCause.Manual;
        invoiceShareList.add(inoviceShare);
    }

    public Set<String> getAuthorizationIdSet(List<Set<String>> authIdList){
        Set<String> authIds = new Set<String>();
        for(Set<String> authSet: authIdList) {
            for(String authId: authSet) {
                authIds.add(authId);
            }
        }
        return authIds;
    }

    public void execute(SchedulableContext sc) {
        NC_InvoiceAuthorizationSharingBatch batchObject = new NC_InvoiceAuthorizationSharingBatch ();
        database.executebatch(batchObject);
    }

    public void finish(Database.BatchableContext bc) {
        //Finish
    }
}