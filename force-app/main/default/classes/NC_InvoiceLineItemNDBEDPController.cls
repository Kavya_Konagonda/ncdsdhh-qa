public with sharing class NC_InvoiceLineItemNDBEDPController {
    
    /*
    * Method Name: getAllLineItems
    * Description: Method to get all the child Invoice Line Items
    * @param: Id recordId - Record Id of Invoic__c Object
    * @return Map<String,Object>
    */
    
    @AuraEnabled
    public static Map<String,Object> getAllLineItems(Id recordId){
        Id CommunicationRecordTypeIdInvoice =  NC_Utility.RecordTypeUtilityMethod('Invoice__c',  NC_Constant.INVOICE_RECORDTYPE_COMMUNICATION);
        Map<String,Object> result = new Map<String,Object>();
        List<LineItemNDBEDPWrapper> lineItemListWrapperList = new List<LineItemNDBEDPWrapper>();
        LineItemNDBEDPWrapper invoiceLineItem;
        Decimal totalAmountToPay = 0;
        //Decimal totalAmountAuthorizedFinal=0;
        Decimal totalAmountBilled = 0;
        //Decimal totalAmountBilledFinal=0;

        for(Invoice_Line_Item__c selectedLineItem :[SELECT id, Name, Equipment_Name__c, Amount_Authorized__c,
                                                    Quantity__c, Amount_Billed__c, Is_Editable__c, Billed_Amount_Editable__c,
                                                    Quantity_to_Pay__c, Amount_to_Pay__c, Is_Quantity_to_Pay_Editable__c, Amount_to_Pay_Editable__c,
                                                    Sequence__c, Invoice__r.RecordTypeId , Maximum_Quantity__c, Invoice__r.Comment__c 
                                                    FROM Invoice_Line_Item__c WHERE Invoice__c =: recordId WITH SECURITY_ENFORCED ORDER BY Sequence__c Asc]){
            Boolean otherCommentVisible = selectedLineItem.Equipment_Name__c == 'Other' ? true : false;
            String otherComment = selectedLineItem.Invoice__r.Comment__c;

            invoiceLineItem = new LineItemNDBEDPWrapper(selectedLineItem.Id, selectedLineItem.Equipment_Name__c, selectedLineItem.Amount_Authorized__c,
            selectedLineItem.Quantity__c, selectedLineItem.Amount_Billed__c == NULL ? 0.0 : selectedLineItem.Amount_Billed__c, selectedLineItem.Is_Editable__c, selectedLineItem.Billed_Amount_Editable__c,
            selectedLineItem.Quantity_to_Pay__c, selectedLineItem.Amount_to_Pay__c == NULL ? 0.0 : selectedLineItem.Amount_to_Pay__c, selectedLineItem.Is_Quantity_to_Pay_Editable__c, selectedLineItem.Amount_to_Pay_Editable__c,
            selectedLineItem.Maximum_Quantity__c!=NULL ? selectedLineItem.Maximum_Quantity__c : 999999999, otherCommentVisible, otherComment, selectedLineItem.Sequence__c);

            lineItemListWrapperList.add(invoiceLineItem);

            selectedLineItem.Amount_Billed__c = selectedLineItem.Amount_Billed__c == NULL ? 0.0 : selectedLineItem.Amount_Billed__c;
            totalAmountBilled += selectedLineItem.Amount_Billed__c;
            //totalAmountAuthorizedFinal = totalAmountAuthorized;
            totalAmountBilled = totalAmountBilled.setScale(2);
            
            selectedLineItem.Amount_to_Pay__c = selectedLineItem.Amount_to_Pay__c == NULL ? 0.0 : selectedLineItem.Amount_to_Pay__c;
            totalAmountToPay += selectedLineItem.Amount_to_Pay__c;
            //totalAmountBilledFinal = totalAmountBilled;
            totalAmountToPay = totalAmountToPay.setScale(2);
        }
        result.put('lineItemListWrapperList',lineItemListWrapperList);
        result.put('totalAmountBilled',totalAmountBilled);
        result.put('totalAmountToPay',totalAmountToPay);
        return result;
    }

    /*
    * Method Name: updateAllLineItems
    * Description: Method to update the Invoice line Items
    * @param: String lineItemData - JSON of all the line Items
    * @return Map<String,Object>
    */
    
    @AuraEnabled
    public static Map<String,Object> updateAllLineItems(String lineItemData, String recordId){
        Map<String,Object> result = new Map<String,Object>();
        try {
            List<LineItemNDBEDPWrapper> lineItemListWrapperList = (List<LineItemNDBEDPWrapper>) JSON.deserialize(lineItemData, List<LineItemNDBEDPWrapper>.class);
            List<Invoice_Line_Item__c> invoiceLineItemToUpdate = new List<Invoice_Line_Item__c>();
            Map<Id,LineItemNDBEDPWrapper> invoiceLineItemIdToWrapperMap = new Map<Id,LineItemNDBEDPWrapper>();
            LineItemNDBEDPWrapper invoiceLineItem;
            Decimal totalAmountBilled = 0.0;
            //Decimal totalAmountBilledFinal=0.0;
            Decimal totalAmountToPay = 0.0;
            String otherComment=null;
            
            Invoice__c invoice = [SELECT Id, Comment__c, Regular_Service_Hours__c, Enhanced_Service_Hours__c, Service_Hours__c, Travel_Time__c, Food__c, Lodging__c, Other__c, Excess_Hotel_Cost__c, Enhanced_Hours__c, Dinner__c, Parking__c, Breakfast__c, Lunch__c, Hotels__c, Mileage__c, Standard_Hours__c FROM Invoice__c WHERE Id =:recordId WITH SECURITY_ENFORCED ];
            for(LineItemNDBEDPWrapper lineItemSelected : lineItemListWrapperList){
                invoiceLineItemIdToWrapperMap.put(lineItemSelected.invoiceLineItemId,lineItemSelected);
            }
            lineItemListWrapperList.clear();

            for(Invoice_Line_Item__c selectedLineItem :[SELECT id, Name, Equipment_Name__c, Amount_Authorized__c,
                                Quantity__c, Amount_Billed__c, Is_Editable__c, Billed_Amount_Editable__c,
                                Quantity_to_Pay__c, Amount_to_Pay__c, Is_Quantity_to_Pay_Editable__c, Amount_to_Pay_Editable__c,
                                Sequence__c, Invoice__r.RecordTypeId , Maximum_Quantity__c, Invoice__r.Comment__c
                            FROM Invoice_Line_Item__c WHERE Id In: invoiceLineItemIdToWrapperMap.keySet() WITH SECURITY_ENFORCED ORDER BY Name Asc]){
                System.debug('lineItemName-->'+invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName);
                if(selectedLineItem.Amount_Billed__c != invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountBilled || selectedLineItem.Amount_to_Pay__c != invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountToPay){
                    if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Parking'){
                        invoice.Parking__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountBilled;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Hotels'){
                        invoice.Hotels__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName.contains('Enhanced')){
                        if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Enhanced Service Hours') {
                            invoice.Enhanced_Service_Hours__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                        } else {
                            invoice.Enhanced_Hours__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                        }
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Regular Service Hours'){
                        invoice.Regular_Service_Hours__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Dinner'){
                        invoice.Dinner__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Lunch'){
                        invoice.Lunch__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Mileage'){
                        invoice.Mileage__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Breakfast'){
                        invoice.Breakfast__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Service Hours'){
                        invoice.Service_Hours__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Travel Time'){
                        invoice.Travel_Time__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName.contains('Standard')){
                        invoice.Standard_Hours__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Food'){
                        invoice.Food__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountBilled;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Lodging'){
                        invoice.Lodging__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountBilled;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Other'){
                        invoice.Other__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountBilled;
                    }else if(invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName == 'Excess hotel Cost'){
                        invoice.Excess_Hotel_Cost__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountBilled;
                    }else{
                    }
                    selectedLineItem.Quantity__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToBill;
                    selectedLineItem.Amount_Billed__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountBilled;
                    selectedLineItem.Quantity_to_Pay__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).quantityToPay; 
                    selectedLineItem.Amount_to_Pay__c = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountToPay;
                }
                System.debug('invoiceLineItemIdToWrapperMap-->'+invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).lineItemName + '-->' + invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).amountToPay);
                totalAmountBilled += selectedLineItem.Amount_Billed__c;
                //totalAmountBilledFinal = totalAmountBilled;
                totalAmountBilled = totalAmountBilled.setScale(2);

                System.debug('selectedLineItem-->'+selectedLineItem.Equipment_Name__c + '-->' + selectedLineItem.Amount_to_Pay__c);
                selectedLineItem.Amount_to_Pay__c = selectedLineItem.Amount_to_Pay__c == null ? 0.0 : selectedLineItem.Amount_to_Pay__c;
                totalAmountToPay += selectedLineItem.Amount_to_Pay__c;
                //totalAmountBilledFinal = totalAmountBilled;
                totalAmountToPay = totalAmountToPay.setScale(2);

                Boolean otherCommentVisible = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).otherCommentVisible;
                if(invoiceLineItemIdToWrapperMap.containsKey(selectedLineItem.Id) && invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).otherComment != null) {
                    otherComment = invoiceLineItemIdToWrapperMap.get(selectedLineItem.Id).otherComment;
                }
                invoiceLineItem = new LineItemNDBEDPWrapper(selectedLineItem.Id, selectedLineItem.Equipment_Name__c, selectedLineItem.Amount_Authorized__c == NULL ? 0.0 : selectedLineItem.Amount_Authorized__c,
                selectedLineItem.Quantity__c, selectedLineItem.Amount_Billed__c == NULL ? 0.0 : selectedLineItem.Amount_Billed__c, selectedLineItem.Is_Editable__c, selectedLineItem.Billed_Amount_Editable__c,
                selectedLineItem.Quantity_to_Pay__c, selectedLineItem.Amount_to_Pay__c == NULL ? 0.0 : selectedLineItem.Amount_to_Pay__c, selectedLineItem.Is_Quantity_to_Pay_Editable__c, selectedLineItem.Amount_to_Pay_Editable__c,
                selectedLineItem.Maximum_Quantity__c != NULL ? selectedLineItem.Maximum_Quantity__c : 999999999, otherCommentVisible, otherComment, selectedLineItem.Sequence__c);

                lineItemListWrapperList.add(invoiceLineItem);
                invoiceLineItemToUpdate.add(selectedLineItem);    
            }
            if(invoiceLineItemToUpdate.size()>0){
                //SObjectAccessDecision decision =  SecurityLibrary.getAccessibleData('Invoice_Line_Item__c', invoiceLineItemToUpdate, 'update', true, 'NC_InvoiceLineItemStaffingController', 'updateAllLineItems');
                //update decision.getRecords();
                if(Schema.SobjectType.Invoice__c.isUpdateable()) {
                 	update invoiceLineItemToUpdate;   
                }
                //invoiceLineItemToUpdate[0].id = decision.getRecords()[0].id;

                invoice.comment__c = otherComment;
                //SObjectAccessDecision decision1 =  SecurityLibrary.getAccessibleData('Invoice__c', new List<Invoice__c>{invoice}, 'update', true, 'NC_InvoiceLineItemStaffingController','updateAllLineItems');
                //update decision1.getRecords();
                // invoice.id = decision1.getRecords()[0].id;
                System.debug('invoice-->'+invoice);
                invoice = replaceZeroToBlank(invoice);
                //Update invoice;
                SObjectAccessDecision decision1 =  SecurityLibrary.getAccessibleData('Invoice__c', new List<Invoice__c>{invoice}, 'update', true ,'NC_InvoiceLineItemNDBEDPController' ,'updateAllLineItems');
                update decision1.getRecords();
                invoice.id = decision1.getRecords()[0].id;

            }
            lineItemListWrapperList.sort();
            result.put('lineItemListWrapperList',lineItemListWrapperList);  
            result.put('totalAmountBilled',totalAmountBilled);
            result.put('totalAmountToPay',totalAmountToPay);
        } catch (Exception e) {
            System.debug('exception-->'+e.getLineNumber()+'-->'+e.getMessage());
            result.put('error', e.getMessage());
        }
        return result;
    }
    
    public static Invoice__c replaceZeroToBlank(Invoice__c invoice) {
        if(invoice.Service_Hours__c == 0) {
            invoice.Service_Hours__c = null;
        }
        if(invoice.Regular_Service_Hours__c == 0) {
            invoice.Regular_Service_Hours__c = null;
        }
        if(invoice.Enhanced_Service_Hours__c == 0) {
            invoice.Enhanced_Service_Hours__c = null;
        }
        if(invoice.Travel_Time__c == 0) {
            invoice.Travel_Time__c = null;
        }
        if(invoice.Mileage__c == 0) {
            invoice.Mileage__c = null;
        }
        if(invoice.Food__c == 0) {
            invoice.Food__c = null;
        }
        if(invoice.Lodging__c == 0) {
            invoice.Lodging__c = null;
        }
        if(invoice.Other__c == 0) {
            invoice.Other__c = null;
        }
        return invoice;
    }
    
    /*
    * Class Name: LineItemNDBEDPWrapper
    * Description: This is a Wrapper class of Invoice line Items
    */
    public class LineItemNDBEDPWrapper implements Comparable{
        @AuraEnabled public String lineItemName;
        @AuraEnabled public Id invoiceLineItemId;
        @AuraEnabled public Decimal amountAuthorized;
        @AuraEnabled public Decimal quantityToBill;
        @AuraEnabled public Decimal amountBilled ;
        @AuraEnabled public Boolean isQuantityToBillEditable;
        @AuraEnabled public Boolean isAmountToBillEditable;
        @AuraEnabled public Decimal quantityToPay;
        @AuraEnabled public Decimal amountToPay ;
        @AuraEnabled public Boolean isQuantityToPayEditable;
        @AuraEnabled public Boolean isAmountToPayEditable;
        @AuraEnabled public Decimal maximumValue;
        @AuraEnabled public Decimal sequnce;
        @AuraEnabled public Boolean otherCommentVisible;
        @AuraEnabled public String otherComment;
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(sequnce - ((LineItemNDBEDPWrapper)objToCompare).sequnce);
        }

        public LineItemNDBEDPWrapper(Id invoiceLineItemId, String lineItemName, Decimal amountAuthorized,
            Decimal quantityToBill, Decimal amountBilled, Boolean isQuantityToBillEditable, Boolean isAmountToBillEditable, 
            Decimal quantityToPay, Decimal amountToPay, Boolean isQuantityToPayEditable, Boolean isAmountToPayEditable, 
            Decimal maximumValue, Boolean otherCommentVisible, String otherComment, Decimal sequnce){
            this.lineItemName = lineItemName;
            this.invoiceLineItemId = invoiceLineItemId;
            this.amountAuthorized = amountAuthorized;
            
            //Quantity and Amount to Billed
            this.quantityToBill = quantityToBill;
            this.amountBilled = amountBilled;
            this.isQuantityToBillEditable = isQuantityToBillEditable;
            this.isAmountToBillEditable = isAmountToBillEditable;
            
            //Quantity and Amount to Pay
            this.quantityToPay = quantityToPay;
            this.amountToPay = amountToPay;
            this.isQuantityToPayEditable = isQuantityToPayEditable;
            this.isAmountToPayEditable = isAmountToPayEditable;
            
            this.maximumValue = maximumValue;   
            this.otherCommentVisible = otherCommentVisible;
            this.otherComment = otherComment;
            this.sequnce = sequnce;
       }
    }
}