/*
*   Class Name: NC_OutreachRequestTriggerHandler
*   Description: Outrech Request Trigger Handler -- Logics on insert, update and delete
*
*   Date            New/Modified         User                 Identifier                Description
*   25/05/2020         New         Shubham Dadhich(mtx)
*/
public with sharing class NC_OutreachRequestTriggerHandler {
    
    public static Id ndbedpEventRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Outreach_Request__c','NDBEDP_Event');
    /*
    * Method Name: beforeUpdate
    * Description: calling this method on 'before Update' trigger event
    * @param: List<Outreach_Request__c> newOutreachRequestList, Map<Id, Outreach_Request__c> oldOutreachRequestMap
    * @return void
    */
    public static void beforeUpdate(List<Outreach_Request__c> newOutreachRequestList, Map<Id, Outreach_Request__c> oldOutreachRequestMap){
        validateRecord(newOutreachRequestList,oldOutreachRequestMap);
        closeOutreachRequest(newOutreachRequestList,oldOutreachRequestMap);
    }

    /*
    * Method Name: validateRecord
    * Description: Validate Record for Submit For Approval record Type Should be DHH_Sponsor_Event
    * @param: List<Outreach_Request__c> newOutreachRequestList, Map<Id, Outreach_Request__c> oldOutreachRequestMap
    * @return void
    */
    public static void validateRecord(List<Outreach_Request__c> newOutreachRequestList, Map<Id, Outreach_Request__c> oldOutreachRequestMap) {
        Id outreachRequestRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Outreach_Request__c','DHH_Sponsor_Event');
        for(Outreach_Request__c outreachRequest : newOutreachRequestList){
            if((outreachRequest.RecordTypeId == outreachRequestRecordTypeId || outreachRequest.recordTypeId == ndbedpEventRecordTypeId) && outreachRequest.Start_Date_of_Event__c != null){
                Integer number_of_Days = System.today().daysBetween(outreachRequest.Start_Date_of_Event__c.date());
                if(outreachRequest.Status__c == NC_Constant.STATUS_PENDING_PUBLIC_COMMUNICATION_REVIEW && !outreachRequest.Public_Communication_Document_Uploaded__c){
                    outreachRequest.addError(NC_Constant.OUTREACH_REQUEST_TRIGGER_ERROR_PUBLIC_COMMUNICATION_UPLOAD);
                }else if(outreachRequest.Status__c == NC_Constant.STATUS_PENDING_PUBLIC_COMMUNICATION_REVIEW &&  !outreachRequest.PA_2_Form_Uploaded__c){
                    outreachRequest.addError(NC_Constant.OUTREACH_REQUEST_TRIGGER_ERROR_PA2FORM);
                }else if((outreachRequest.Status__c == NC_Constant.STATUS_INITIAL_REQUEST_REVIEW || outreachRequest.Status__c == NC_Constant.STATUS_PENDING_PUBLIC_COMMUNICATION_REVIEW || outreachRequest.Status__c == NC_Constant.STATUS_PENDING_EVENT_APPROVAL || outreachRequest.Status__c == NC_Constant.STATUS_PUBLIC_COMMUNICATION_DIRECTOR_REVIEW) && number_of_Days < 45){
                    if(outreachRequest.Justification__c == null){
                        outreachRequest.addError(NC_Constant.OUTREACH_REQUEST_TRIGGER_ERROR_JUSTIFICATION_NOT_NULL);
                    }
                }
            }
        }
    }
     /*
    * Method Name: closeOutreachRequest
    * Description: 
    * @param: List<Outreach_Request__c> newList, Map<Id, Outreach_Request__c> oldMap
    * @return void
    */
    public static void closeOutreachRequest(List<Outreach_Request__c> newList, Map<Id, Outreach_Request__c> oldMap) {
        Id outreachRequestRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Outreach_Request__c','DHH_Sponsor_Event');
        List<Outreach_Request__c> outreachRequestList = new List<Outreach_Request__c>();
        Set<Id> outreachWithAttendeeSet = new Set<Id>();
        for(Outreach_Request__c outreachRequest : newList){
            if(oldMap != null && oldMap.get(outreachRequest.Id).Status__C != outreachRequest.Status__C &&
               (outreachRequest.RecordTypeId == outreachRequestRecordTypeId || outreachRequest.recordTypeId == ndbedpEventRecordTypeId) && outreachRequest.Status__C == 'Closed'){
                outreachRequestList.add(outreachRequest);
            }
        }
        if(outreachRequestList.size()>0){
            for(Attendees__c attendeeRecord : [SELECT id, Outreach_Request__c FROM Attendees__c WHERE Outreach_Request__c IN: outreachRequestList WITH SECURITY_ENFORCED]){
                outreachWithAttendeeSet.add(attendeeRecord.Outreach_Request__c);
            }
            for(Outreach_Request__c outreachRequest : outreachRequestList){
                if(!outreachWithAttendeeSet.contains(outreachRequest.Id)){
                    outreachRequest.addError('Atleast one Attendee is needed before closing Outreach');
                }
            }
        }
       
    }
}