/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 06-21-2021
 * @last modified by  : kavya.konagonda@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   06-10-2021   kavya.konagonda@mtxb2b.com   Initial Version
**/
public with sharing class NC_ProductDataFactory {
     public static List<Product2> createProductRecords(Integer count, Boolean isInsert) {
        List<Product2> productList = new List<Product2>();
        List<String> productNames = new List<String>{'Base Plate', 'Surcharge', 'Pickup Costs', 'Labor Costs', 'Delivery Costs'};
            
            for(Integer i=0; i<count; i++) {
                Product2 productRecord = new Product2();
                productRecord.Name = productNames.get(Math.mod(i, 5));
                productList.add(productRecord);
            }
        if(isInsert) {
            SObjectAccessDecision objectDecision = SecurityLibrary.getAccessibleData('Product2', productList, 'insert', true,'NC_ProductDataFactory','createProductRecords');
            insert objectDecision.getRecords();
        }
        return productList;
    }
}