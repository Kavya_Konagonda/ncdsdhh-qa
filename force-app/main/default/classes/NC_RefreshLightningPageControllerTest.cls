@isTest
public with sharing class NC_RefreshLightningPageControllerTest {
    
    static testmethod void checkPageRefreshTest() {

        Case caseObj = new Case();
        caseObj.Refresh_Page__c = true;
        insert caseObj;
        
        Boolean result = NC_RefreshLightningPageController.checkPageRefresh(caseObj.id);
        System.assertEquals(true, result);
    }
}