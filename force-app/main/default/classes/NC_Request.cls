public with sharing class NC_Request {

    @AuraEnabled
    public static String getConsumerName(String contactId){
        String parentContactId = '';
        if(String.isNotBlank(contactId)){
            parentContactId = [SELECT Id, Name FROM Contact WHERE Id=:contactId LIMIT 1].Name;
            return parentContactId;
        }
        return null;
    }
    @AuraEnabled
    public static String getRecordTypeId(String requestRecordTypeName){
        String recordTypeId = '';
        if(String.isNotBlank(requestRecordTypeName)){
            recordTypeId = [SELECT Id, DeveloperName, Name, SobjectType FROM RecordType WHERE DeveloperName = :requestRecordTypeName AND SobjectType='Case'].Id;
            return recordTypeId;
        }
        return null;
    }
    @AuraEnabled
    public static Map<Id,Boolean> saveData(String dataObj){
        Map<Id,Boolean> alreadyPresentCaseMap = new Map<Id,Boolean>();
        List<Case> alreadyPresentCaseList = new List<Case>();
        Id ndbedpRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Case','NDBEDP');
        Id edsRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Case','Equipment');
        List<String> statusList = new List<String>{'Approved','Denied','Rejected'};
        try {
            RequestDataWrapper requestWrapper = (RequestDataWrapper) JSON.deserialize(dataObj, RequestDataWrapper.class);
            System.debug('requestWrapper data is '+requestWrapper);
            Case caseItem = requestWrapper.RequesDataHandler();
            System.debug('caseItem'+caseItem);
            if(edsRecordTypeId != caseItem.recordTypeId){
                alreadyPresentCaseList =[SELECT Id,Status FROM Case WHERE Status NOT IN:statusList AND RecordTypeId =:ndbedpRecordTypeId AND ContactId=:caseItem.ContactId WITH SECURITY_ENFORCED];
           // System.debug('alreadyPresentCase'+alreadyPresentCase);
            if(alreadyPresentCaseList.size()>0){
                alreadyPresentCaseMap.put(alreadyPresentCaseList[0].Id,false);
                return alreadyPresentCaseMap;
            }
            else{
            //insert caseItem;
            SObjectAccessDecision decision = SecurityLibrary.getAccessibleData('Case', new List<Case>{caseItem}, 'insert', true,'NC_Request','saveData');
            insert decision.getRecords();
            caseItem = (Case)decision.getRecords()[0];
            alreadyPresentCaseMap.put(caseItem.Id,true);
            return alreadyPresentCaseMap;
            }
         }
         else{
            //insert caseItem;
            SObjectAccessDecision decision = SecurityLibrary.getAccessibleData('Case', new List<Case>{caseItem}, 'insert', true,'NC_Request','saveData');
            insert decision.getRecords();
            caseItem = (Case)decision.getRecords()[0];
            alreadyPresentCaseMap.put(caseItem.Id,true);
            return alreadyPresentCaseMap;
         }
        }
        catch (Exception e) {
              System.debug('errorr'+e.getMessage()+e.getLineNumber());  
              System.debug('msg'+e.getStackTraceString());
              throw new AuraHandledException('errorr'+e.getMessage()+e.getLineNumber());  
        }

   }
    public class RequestDataWrapper{
        @AuraEnabled public string requestType;
        @AuraEnabled public string contactId;
        @AuraEnabled public string consumerName;
        @AuraEnabled public List<string> EquipmentTypes;

        public Case RequesDataHandler() {
            Case caseValue = new Case();
            caseValue.ContactId=contactId;
           // caseValue.Equipment_Types__c = String.join(EquipmentTypes, ';');
            caseValue.Status= 'Submitted';
            if(requestType == 'EDS'){
                caseValue.Request_Type__c ='EDS';
                caseValue.Equipment_Types__c = String.join(EquipmentTypes, ';');
                caseValue.recordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Equipment').getRecordTypeId();
            }
            else if(requestType == 'NDBEDP'){
                caseValue.Request_Type__c ='NDBEDP';
                caseValue.recordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NDBEDP').getRecordTypeId();
            }
            System.debug('caseValue'+caseValue);
            return caseValue;
    }
    
}
}