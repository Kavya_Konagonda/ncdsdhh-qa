/*
*   Class Name: NC_SendBulkEmail
*   Description: Bul Email Handler
*
*   Date            New/Modified         User                 Identifier                Description
*   22/05/2020         New         Shubham Dadhich(mtx)
*/
public with sharing class NC_SendBulkEmail {

    /*
    * Method Name: sendEmail
    * Description: send Email Method to send Bulk Emails to related Consumers in future Callout
    * @param: Set<Id> consumerSetId
    * @return:
    */
    @future(callout=true)
    public static void sendEmail(Set<Id> setOfAuthorizationId) {
        Set<Id> setOfAuthorizationNonHearingAid = new Set<Id>();
        Set<Id> setOfAuthorizationHearingAid = new Set<Id>();
        Map<Id, Id> mapOfContactIdWithAuth = new Map<Id, Id>();
        Map<Id, String> mapOfAuthWithEmail = new Map<Id, String>();
        Map<Id, String> mapofAssetWithConsumer = new Map<Id, String>();
        Map<Id, String> mapofAssetWithRegionalCenter = new Map<Id, String>();
        Map<Id, String> mapofAssetWithVendor = new Map<Id, String>();
        Map<Id, String> mapOfAuthIdWithRecordtypeName = new Map<Id, String>();
        Map<Id, String> mapOfAuthIdWithRequestRecordtypeName = new Map<Id, String>();
        Id requestRecordId = NC_Utility.RecordTypeUtilityMethod('Case','NDBEDP');
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();

        for(Asset__c assetRecord : [SELECT Id,Authorization__r.Recordtype.DeveloperName,Authorization__r.Request__r.RecordtypeId, Authorization__r.Vendor__r.Email__c,Authorization__c, Authorization__r.Consumer__c,Authorization__r.Consumer__r.Name, Authorization__r.Vendor__r.Name,Authorization__r.Regional_Center__r.Name, Equipment__c, Equipment__r.Equipment_Type__c, Equipment__r.Is_Sub_Equipment__c FROM Asset__c WHERE Authorization__c IN : setOfAuthorizationId WITH SECURITY_ENFORCED]){
            if(assetRecord.Equipment__r.Equipment_Type__c == NC_Constant.EQUIPMENT_TYPE_HEARING_AID && !assetRecord.Equipment__r.Is_Sub_Equipment__c){
                setOfAuthorizationHearingAid.add(assetRecord.Authorization__c);
                mapOfContactIdWithAuth.put(assetRecord.Authorization__c, assetRecord.Authorization__r.Consumer__c);
                mapOfAuthWithEmail.put(assetRecord.Authorization__c, assetRecord.Authorization__r.Vendor__r.Email__c);
            }else{
                setOfAuthorizationNonHearingAid.add(assetRecord.Authorization__c);
                mapOfContactIdWithAuth.put(assetRecord.Authorization__c, assetRecord.Authorization__r.Consumer__c);
                mapOfAuthWithEmail.put(assetRecord.Authorization__c, assetRecord.Authorization__r.Vendor__r.Email__c);
            }
            mapofAssetWithConsumer.put(assetRecord.Authorization__c, assetRecord.Authorization__r.Consumer__r.Name);
            mapofAssetWithRegionalCenter.put(assetRecord.Authorization__c, assetRecord.Authorization__r.Regional_Center__r.Name);
            mapofAssetWithVendor.put(assetRecord.Authorization__c, assetRecord.Authorization__r.Vendor__r.Name);
            mapOfAuthIdWithRecordtypeName.put(assetRecord.Authorization__c,assetRecord.Authorization__r.Recordtype.DeveloperName);
            mapOfAuthIdWithRequestRecordtypeName.put(assetRecord.Authorization__c,assetRecord.Authorization__r.Request__r.RecordtypeId);
        }

        // for(Authorization__c auth :[SELECT Id,Recordtype.DeveloperName,Request__r.RecordtypeId FROM Authorization__c WHERE Id IN:setOfAuthorizationId]){
        //     mapOfAuthIdWithRecordtypeName.put(auth.Id,auth.Recordtype.DeveloperName);
        //     mapOfAuthIdWithRequestRecordtypeName.put(auth.Id,auth.Request__r.RecordtypeId);
        // }
        System.debug('mapOfAuthIdWithRecordtypeName'+mapOfAuthIdWithRecordtypeName);
        System.debug('mapOfAuthIdWithRequestRecordtypeName'+mapOfAuthIdWithRequestRecordtypeName);
        //System.debug('setOfAuthorizationNonHearingAid'+setOfAuthorizationNonHearingAid);


        for(Id recordId : setOfAuthorizationId){
            // System.debug(' mapOfAuthIdWithRecordtypeName.get(recordId) == requestRecordId'+mapOfAuthIdWithRecordtypeName.get(recordId) == 'Equipment' && mapOfAuthIdWithRequestRecordtypeName.get(recordId) == requestRecordId);
            // System.debug(  mapOfAuthIdWithRequestRecordtypeName.get(recordId) == requestRecordId);
            // System.debug(' mapOfAuthIdWithRecordtypeName.get(recordId) == requestRecordId22'+mapOfAuthIdWithRecordtypeName.get(recordId) == 'Equipment');
                        if(mapOfAuthIdWithRecordtypeName.get(recordId) == 'Equipment' && mapOfAuthIdWithRequestRecordtypeName.get(recordId) == requestRecordId ){
                            List<Messaging.EmailFileAttachment> listofEmailAttachments = new List<Messaging.EmailFileAttachment>();
                            if(setOfAuthorizationNonHearingAid.contains(recordId)){
                                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment(); 
                                PageReference ref= Page.NC_WeatherRadioApprovalConsumer;
                                ref.getParameters().put('id',recordId);
                                try{
                                    Blob blobValue = ref.getContentAsPDF();
                                    emailAttachment.setFileName('Equipment_Authorization.pdf');
                                    emailAttachment.setBody(blobValue);
                                    listofEmailAttachments.add(emailAttachment);
                                    System.debug('123455');
                                }catch(Exception e){
                                }
                            }
                            Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
                            singleEmailMessage.setSubject( NC_Constant.CONSUMER_SUBJECT_AUTHORIZATION +String.valueOf(DateTime.now()));
                            singleEmailMessage.setTargetObjectId(mapOfContactIdWithAuth.get(recordId));
                            singleEmailMessage.setPlainTextBody('Your application for (equipment(s)) have been approved.  Your approval letter is attached.  Please keep it for your files.  Please contact your '+ mapofAssetWithRegionalCenter.get(recordId)+' if you have any questions or concerns and use this letter as reference.');
                            singleEmailMessage.setFileAttachments(listofEmailAttachments);
                            // singleEmailMessage.setToAddresses(new List<String>{'saisujith.koppula@mtxb2b.com'});
                          //  System.debug('singleEmailMessage'+ singleEmailMessage);
                            System.debug('123455');
                            singleEmailMessageList.add(singleEmailMessage);


                        }else{
                        //Consumer Email Send
                        Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
                        List<Messaging.EmailFileAttachment> listofEmailAttachments = new List<Messaging.EmailFileAttachment>();
                        if(setOfAuthorizationNonHearingAid.contains(recordId)){
                            Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment(); 
                            PageReference ref= Page.NC_WeatherRadioApprovalConsumer;
                            ref.getParameters().put('id',recordId);
                            try{
                                Blob blobValue = ref.getContentAsPDF();
                                emailAttachment.setFileName('Equipment_Authorization.pdf');
                                emailAttachment.setBody(blobValue);
                                listofEmailAttachments.add(emailAttachment);
                            }catch(Exception e){
                            }
                            
                        }
                        if(setOfAuthorizationHearingAid.contains(recordId)){
                            Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment(); 
                            PageReference ref= Page.NC_HA_ConsumerApproval;
                            ref.getParameters().put('id',recordId);
                            try{
                                Blob blobValue = ref.getContentAsPDF();
                                emailAttachment.setFileName('Hearing_Aid_Authorization.pdf');
                                emailAttachment.setBody(blobValue);
                                listofEmailAttachments.add(emailAttachment);
                            }catch(Exception e){
                            }
                            
                        }
                        
                        singleEmailMessage.setSubject( NC_Constant.CONSUMER_SUBJECT_AUTHORIZATION +String.valueOf(DateTime.now()));
                        singleEmailMessage.setTargetObjectId(mapOfContactIdWithAuth.get(recordId));
                        singleEmailMessage.setPlainTextBody('Your application for (equipment(s)) have been approved.  Your approval letter is attached.  Please keep it for your files.  Please contact your '+ mapofAssetWithRegionalCenter.get(recordId)+' if you have any questions or concerns and use this letter as reference.');
                        singleEmailMessage.setFileAttachments(listofEmailAttachments);
                        // singleEmailMessage.setToAddresses(new List<String>{'saisujith.koppula@mtxb2b.com'});
                        System.debug('singleEmailMessage'+ singleEmailMessage);
                        singleEmailMessageList.add(singleEmailMessage);

                        //vendor Email Send
                        Messaging.SingleEmailMessage singleEmailMessageVendor = new Messaging.SingleEmailMessage();
                        List<Messaging.EmailFileAttachment> listofEmailAttachmentsVendor = new List<Messaging.EmailFileAttachment>();
                        if(setOfAuthorizationNonHearingAid.contains(recordId)){
                            Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment(); 
                            PageReference ref= Page.NC_EquipmentAuthorization;
                            ref.getParameters().put('id',recordId);
                            try{
                                Blob blobValue = ref.getContentAsPDF();
                                emailAttachment.setFileName('Equipment_Authorization.pdf');
                                emailAttachment.setBody(blobValue);
                                listofEmailAttachmentsVendor.add(emailAttachment);
                            }catch(Exception e){
                            }
                            
                        }
                        if(setOfAuthorizationHearingAid.contains(recordId)){
                            Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment(); 
                            PageReference ref= Page.NC_HA_Authorization;
                            ref.getParameters().put('id',recordId);
                            try{
                                Blob blobValue = ref.getContentAsPDF();
                                emailAttachment.setFileName('Hearing_Aid_Authorization.pdf');
                                emailAttachment.setBody(blobValue);
                                listofEmailAttachmentsVendor.add(emailAttachment);
                            }catch(Exception e){
                            }
                            
                        }
                        
                        singleEmailMessageVendor.setSubject( NC_Constant.VENDOR_SUBJECT_AUTHORIZATION +String.valueOf(DateTime.now()));
                        singleEmailMessageVendor.setToAddresses(new List<String>{mapOfAuthWithEmail.get(recordId)});
                        singleEmailMessageVendor.setPlainTextBody('Dear '+mapofAssetWithVendor.get(recordId)+', Attached is the official authorization for equipments for '+mapofAssetWithConsumer.get(recordId)+'.  Please contact your '+mapofAssetWithRegionalCenter.get(recordId)+' for any questions about this authorization.');
                        singleEmailMessageVendor.setFileAttachments(listofEmailAttachmentsVendor);
                        singleEmailMessageList.add(singleEmailMessageVendor);
                    }
            }
        

        // Send Email
        Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);

    }

    /*
    * Method Name: sendEmail
    * Description: send Email Method to send Bulk Emails to related Consumers in future Callout
    * @param: Set<Id> consumerSetId
    * @return:
    */
    @future(callout=true)
    public static void sendEmailForExternalAuthorization(Set<Id> setOfAuthorizationId) {
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();
        Map<Id, String> mapOfAuthWithEmail = new Map<Id, String>();

        for(Authorization__c auth : [SELECT Vendor__c, vendor__r.Email__c, Vendor__r.Preferred_Method_of_Contact__c FROM Authorization__c WHERE Id IN: setOfAuthorizationId WITH SECURITY_ENFORCED]){
            if(auth.Vendor__r.Preferred_Method_of_Contact__c == NC_Constant.EMAIL_PPREFERRED_CONTACT){
                mapOfAuthWithEmail.put(auth.Id, auth.vendor__r.Email__c);
            }
        }
        
        for(Id recordId : setOfAuthorizationId){
            if(mapOfAuthWithEmail.containsKey(recordId)){
                //Consumer Email Send
                Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
                List<Messaging.EmailFileAttachment> listofEmailAttachments = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment(); 
                PageReference ref= Page.NC_InterpreterAuthorizationDocument;
                try{
                    Blob blobValue = ref.getContentAsPDF();
                    emailAttachment.setFileName('Authorization.pdf');
                    emailAttachment.setBody(blobValue);
                    listofEmailAttachments.add(emailAttachment);
                }catch(Exception e){
                }
                singleEmailMessage.setSubject(NC_Constant.EXTERNAL_AUTHORIZATION_SUBJECT +String.valueOf(DateTime.now()));
                singleEmailMessage.setToAddresses(new List<String>{mapOfAuthWithEmail.get(recordId)});
                singleEmailMessage.setPlainTextBody(NC_Constant.EXTERNAL_AUTHORIZATION_BODY);
                singleEmailMessage.setFileAttachments(listofEmailAttachments);
                singleEmailMessageList.add(singleEmailMessage);
            }
        }

        if(!singleEmailMessageList.isEmpty()){
            Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);
        }

    }


    /*
    * Method Name: sendEmail
    * Description: send Email Method to send Bulk Emails to related Consumers in future Callout
    * @param: Set<Id> consumerSetId
    * @return:
    */
    @future(callout=true)
    public static void sendEmailOnRequestDenial(Map<Id,Id> mapOfConsumerWithCase) {
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();

        for(Id recordId : mapOfConsumerWithCase.keySet()){
            Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
            Messaging.EmailFileAttachment EmailAttachments = new Messaging.EmailFileAttachment();
            PageReference ref= Page.NC_DenialLetterForEquipment;
            ref.getParameters().put('id',recordId);
            try{
                Blob blobValue = ref.getContentAsPDF();
                EmailAttachments.setFileName('Denial_Letter_Equipment.pdf');
                EmailAttachments.setBody(blobValue);
            }catch(Exception e){
            }
            singleEmailMessage.setSubject( NC_Constant.REQUEST_DENIED_EMAIL_SUBJECT + String.valueOf(DateTime.now()));
            singleEmailMessage.setTargetObjectId(mapOfConsumerWithCase.get(recordId));
            singleEmailMessage.setPlainTextBody(NC_Constant.REQUEST_DENIED_EMAIL_BODY);
            singleEmailMessage.setFileAttachments(new List<Messaging.EmailFileAttachment>{EmailAttachments});
            singleEmailMessageList.add(singleEmailMessage);
        }
        Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);

    }
    
   /*
    * Method Name: semdEmailToGroups
    * Description: send email to a group of users
    * @param: Map<String,Set<Id>> groupNameToRecordIdListMap
    * @return:
    */

    public static void semdEmailToGroups(Map<String,Set<Id>> groupNameToRecordIdListMap, String templateApiName){
        Map<String,Id> groupNameAndIdMap = new Map<String,Id>();
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();
        Id templateId = [select id,name from EmailTemplate where developername = : templateApiName].id;
        Map<Id, List<Id>> mapOfGroupIdWithUsers = new Map<Id, List<Id>>();
        Messaging.SingleEmailMessage singleEmailMessage;
        
        for(Group publicGroup : [SELECT Id, Name, DeveloperName 
                                 FROM Group WHERE Type =: NC_Constant.PUBLIC_GROUP_TYPE 
                                 AND DeveloperName IN: groupNameToRecordIdListMap.keySet() WITH SECURITY_ENFORCED]) {
                                     groupNameAndIdMap.put(publicGroup.DeveloperName,publicGroup.Id);
                                 }
        
        for(GroupMember groupmemberRecord : [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId IN: groupNameAndIdMap.values() WITH SECURITY_ENFORCED]){
            if(!mapOfGroupIdWithUsers.containsKey(groupmemberRecord.GroupId)){
                mapOfGroupIdWithUsers.put(groupmemberRecord.GroupId, new List<Id>());
            }
            mapOfGroupIdWithUsers.get(groupmemberRecord.GroupId).add(groupmemberRecord.UserOrGroupId);
        }
        
        for(String groupName : groupNameAndIdMap.keySet()){
            if(groupNameToRecordIdListMap.containsKey(groupName)){
                for(Id recordId : groupNameToRecordIdListMap.get(groupName)){
                    if(mapOfGroupIdWithUsers.containskey(recordId)){
                        for(Id userId: mapOfGroupIdWithUsers.get(recordId)){
                            singleEmailMessage = Messaging.renderStoredEmailTemplate(templateId,userId,recordId);
                            singleEmailMessage.setTargetObjectId(userId);
                            singleEmailMessage.setSaveAsActivity(false);
                            singleEmailMessageList.add(singleEmailMessage); 
                        } 
                    }
                    
                }
            }
        }  
        Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);
    }
    
    
    /*
    * Method Name: semdEmailToGroups
    * Description: send email to a group of users
    * @param: Map<Id,Id> userIdToRecordIdMap, String emailTemplate
    * @return:
    */
    public static void sendEmailToUser(Map<Id,Set<Id>> userIdToRecordIdMap, String templateApiName){
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();
        Id templateId = [select id,name from EmailTemplate where developername = : templateApiName].id;
        Messaging.SingleEmailMessage singleEmailMessage;
        for(Id userId : userIdToRecordIdMap.keySet()){
            for(Id recordId : userIdToRecordIdMap.get(userId)){
                singleEmailMessage = Messaging.renderStoredEmailTemplate(templateId,userId,recordId);
                singleEmailMessage.setTargetObjectId(userId);
                singleEmailMessage.setSaveAsActivity(false);
                singleEmailMessageList.add(singleEmailMessage);
            }
        }
        Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);
    }

    /*
    * Method Name: notifieBudgetManagerOnBudgetLow
    * Description: send Email Method to send Bulk Emails to budget Managers
    * @param: Set<Id> consumerSetId
    * @return:
    */
    public static void notifieBudgetManagerOnBudgetLow(List<NC_BudgetManagementController.budgetNotifier> budgetNotfierList) {
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();

        for(NC_BudgetManagementController.budgetNotifier budgetNotifierRecord : budgetNotfierList){
            Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
            //singleEmailMessage.setSubject('Low Budget, Please review!');
            singleEmailMessage.setSubject('Budget for '+budgetNotifierRecord.budgetName +' needs review');
            singleEmailMessage.setTargetObjectId(budgetNotifierRecord.UserId);
            //singleEmailMessage.setHtmlBody(budgetNotifierRecord.budgetMessage + budgetNotifierRecord.budgetURL);
            singleEmailMessage.setHtmlBody(budgetNotifierRecord.budgetMessage);
            singleEmailMessage.setSaveAsActivity(false);
            singleEmailMessageList.add(singleEmailMessage);
        }
        Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);

    }

    /*
    * Method Name: notifieBudgetManagerOnBudgetLow
    * Description: send Email Method to send Bulk Emails to budget Managers
    * @param: Set<Id> consumerSetId
    * @return:
    */
    public static void notifieResourceLoanOwner(Map<Id, String> mapOfOwnerWithMessage) {
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();

        for(Id OwnerId : mapOfOwnerWithMessage.keySet()){
            Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
            singleEmailMessage.setSubject('Resource available');
            singleEmailMessage.setTargetObjectId(OwnerId);
            singleEmailMessage.setHtmlBody(mapOfOwnerWithMessage.get(OwnerId));
            singleEmailMessage.setSaveAsActivity(false);
            singleEmailMessageList.add(singleEmailMessage);
        }
        Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);

    }
    
     /*
    * Method Name: notifieOwnerForDifferentRegionalCenter
    * Description: send Email Method to notify the user for different regional center.
    * @param: Map<Asset__c,String> assetToRegionalCenterMap
    * @return:
    */
    public static void notifieOwnerForDifferentRegionalCenter(Map<Id,Case> assetToNewRequestMap,List<Asset__c> assetToSendRecordList){
        List<Messaging.SingleEmailMessage> singleEmailMessageList = new List<Messaging.SingleEmailMessage>();
        Map<Id,String> regionalCenterIdToNameMap = NC_Utility.getRegionalCenterIdToNameMap();
        for(Asset__c assetRecord : assetToSendRecordList){
            Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
            singleEmailMessage.setSubject('Asset Reassigned');
            singleEmailMessage.setTargetObjectId(assetRecord.Request__r.OwnerId);
            singleEmailMessage.setHtmlBody('The item with Asset number '+ assetRecord.Name +' assigned to complete the Request number '+assetToNewRequestMap.get(assetRecord.Id).CaseNumber +' is being reassigned from '+ regionalCenterIdToNameMap.get(assetRecord.Request__r.Regional_Center__c) +'. Please contact '+ regionalCenterIdToNameMap.get( assetToNewRequestMap.get(assetRecord.Id).Regional_Center__c) +'  to confirm the location of item, and arrange for shipment to your Regional Center.');
            singleEmailMessage.setSaveAsActivity(false);
            singleEmailMessageList.add(singleEmailMessage);
        }
        Messaging.SendEmailResult [] result = Messaging.sendEmail(singleEmailMessageList);
    }
    
    
}