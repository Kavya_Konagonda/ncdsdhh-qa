public without sharing class NC_ServiceRequestHelper {
    
    public static Case saveData(Case caseObj){
        try {
            insert caseObj;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return caseObj;
    }
}