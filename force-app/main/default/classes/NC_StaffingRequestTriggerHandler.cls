/*
**   Class Name: NC_StaffingRequestTriggerHandler
**   Description: 
**
**     Date            New/Modified           User                 Identifier                Description
**  06-04-2020             New              Hiten Aggarwal(mtx)                                 
**  06-14-2020             Update           Shubham Dadhich(mtx)                             Added Roll Up To Communicaiton Acces Total_Internal_Cost__c
*/


public with sharing class NC_StaffingRequestTriggerHandler {

   /*
    * Method Name: afterUpdate
    * Description: calling this method on 'after UPDATE' trigger event
    * @param: List<Staffing_Request__c> newList
    * @return void
    */
    public static void afterUpdate(List<Staffing_Request__c> newList, Map<Id,Staffing_Request__c> oldMap){
       checkTotalStaffingUserRequest(newList,oldMap);
    }
           /*
    * Method Name: beforeUpdate
    * Description: calling this method on 'before UPDATE' trigger event
    * @param: List<Staffing_Request__c> newList
    * @return void
    */
    public static void beforeUpdate(List<Staffing_Request__c> newList, Map<Id,Staffing_Request__c> oldMap){
       //before update
    }



    /*
    * Method Name: rollUpOnInsertUpdateUnDelete
    * Description: Roll Up On Insert Update and Undelete
    * @param: List<Staffing_Request__C> listOfStaffingNew, Map<Id, Staffing_Request__C> oldMap
    * @return void
    */
    public static void rollUpOnInsertUpdateUnDelete(List<Staffing_Request__c> listOfStaffingNew, Map<Id, Staffing_Request__c> oldMap){
        Id StaffingInternal = NC_Utility.RecordTypeUtilityMethod('Staffing_Request__c', NC_Constant.STAFFING_REQUEST_INTERNAL);
        List<Staffing_Request__c> listOfStaffingRequest = new List<Staffing_Request__c>();
        for(Staffing_Request__c staffing : listOfStaffingNew){
            if(oldMap != null && staffing.RecordTypeId == StaffingInternal && staffing.Total_Internal_Cost__c != oldMap.get(staffing.Id).Total_Internal_Cost__c){
                listOfStaffingRequest.add(staffing);
            }
        }
        if(!listOfStaffingRequest.isEmpty()){
            List<NC_RollupUtility.fieldDefinition> fieldDefinitions = new list<NC_RollupUtility.fieldDefinition> {new NC_RollupUtility.fieldDefinition('SUM', 'Total_Internal_Cost__c','CACL_Total_Cost__c')};
            NC_RollupUtility.rollUpTrigger(fieldDefinitions, listOfStaffingRequest, 'Staffing_Request__c', 'Communication_Access_Request__c', 'Communication_Access_Request__c', '');
        }
    }

    /*
    * Method Name: rollUpOnDelete
    * Description: Roll Up On Delete
    * @param: List<Staffing_Request__c> listOfStaffingNew
    * @return void
    */
    public static void rollUpOnDelete(List<Staffing_Request__c> listOfStaffingNew){
        Id StaffingInternal = NC_Utility.RecordTypeUtilityMethod('Staffing_Request__c', NC_Constant.STAFFING_REQUEST_INTERNAL);
        List<Staffing_Request__c> listOfStaffingRequest = new List<Staffing_Request__c>();
        for(Staffing_Request__c staffing : listOfStaffingNew){
            if(staffing.RecordTypeId == StaffingInternal){
                listOfStaffingRequest.add(staffing);
            }
        }
        if(!listOfStaffingRequest.isEmpty()){
            List<NC_RollupUtility.fieldDefinition> fieldDefinitions = new list<NC_RollupUtility.fieldDefinition> {new NC_RollupUtility.fieldDefinition('SUM', 'Total_Internal_Cost__c','CACL_Total_Cost__c')};
            NC_RollupUtility.rollUpTrigger(fieldDefinitions, listOfStaffingRequest, 'Staffing_Request__c', 'Communication_Access_Request__c', 'Communication_Access_Request__c', '');
        }
    }


    /*
    * Method Name: checkTotalStaffingUserRequest
    * Description: List<Staffing_Request__c> newList, Map<Id,Staffing_Request__c> oldMap
    * @param: List<Staffing_Request__c> newList
    * @return void
    */
   private static void checkTotalStaffingUserRequest(List<Staffing_Request__c> newList, Map<Id,Staffing_Request__c> oldMap){
        Id internalRecordTypeId = NC_Utility.RecordTypeUtilityMethod('Staffing_Request__c', 'Internal_Staff');
        Set<Id> staffingRequestIdSet = new Set<Id>();
        List<Staffing_User_Request__c> staffingUserRequestUpdateList = new List<Staffing_User_Request__c>();
        Map<Id,Id> processInstanceTostaffingUserRequest = new Map<Id,Id>();
        List<Staffing_User_Request__c> staffingUserRequestList = new List<Staffing_User_Request__c>();
        Map<Id,Id> ProcessInstanceIdToProcessInstanceWorkitemMap = new Map<Id,Id>();
        for(Staffing_Request__c staffingRequest : newList){
            if(oldMap != NULL && (staffingRequest.Number_of_Vendors_Needed__c != NULL || staffingRequest.Number_of_Interpreters_Needed__c != NULL)){
                if(staffingRequest.RecordTypeId == internalRecordTypeId){
                    if(staffingRequest.Total_Members_Attending__c >= Integer.valueOf(staffingRequest.Number_of_Interpreters_Needed__c)){
                        staffingRequestIdSet.add(staffingRequest.Id);
                    }  
                }else{
                    if(staffingRequest.Total_Members_Attending__c >= Integer.valueOf(staffingRequest.Number_of_Vendors_Needed__c)){
                        staffingRequestIdSet.add(staffingRequest.Id);
                    }
                }
            }
        }
        for(Staffing_User_Request__c staffingUserRequest : [Select Id,Status__c, Request_Accepted__c 
                                                            From Staffing_User_Request__c 
                                                            WHERE Staffing_Request__c IN: staffingRequestIdSet
                                                            AND (Status__c = 'Pending Staff Approval' OR Status__c = 'Pending Approval' 
                                                                 OR Status__c = 'Pending Vendor Approval' OR Status__c = 'Request Accepted') WITH SECURITY_ENFORCED]){
           
            staffingUserRequestList.add(staffingUserRequest);                                                         
        }
 
        if(staffingUserRequestList.size()>0){
            /*Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId 
                                                          FROM ProcessInstance 
                                                          where Status='Pending' 
															AND TargetObjectId in : staffingUserRequestIdSet])).keySet();*/
            for(ProcessInstance process : [SELECT Id,Status,TargetObjectId 
                                           FROM ProcessInstance 
                                           where Status='Pending' 
                                           AND TargetObjectId in : staffingUserRequestList WITH SECURITY_ENFORCED]){
                                               if(!processInstanceTostaffingUserRequest.containsKey(process.Id)){
                                                   processInstanceTostaffingUserRequest.put(process.Id,process.TargetObjectId);
                                               }
                                           }
            
            for(ProcessInstanceWorkitem processInstanceWorkItems:  [SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId In : processInstanceTostaffingUserRequest.keySet() WITH SECURITY_ENFORCED]){
                ProcessInstanceIdToProcessInstanceWorkitemMap.put(processInstanceWorkItems.ProcessInstanceId, processInstanceWorkItems.Id);
            }
                    	
            for (Id pInstanceWorkitemsId: ProcessInstanceIdToProcessInstanceWorkitemMap.values()){
                Approval.ProcessWorkitemRequest singleApprovalProcess = new Approval.ProcessWorkitemRequest();
                singleApprovalProcess.setComments('All Slots Filled.');
                singleApprovalProcess.setAction('Reject');
                singleApprovalProcess.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                singleApprovalProcess.setWorkitemId(pInstanceWorkitemsId);
                Approval.ProcessResult result = Approval.process(singleApprovalProcess); 
            }
            for(Staffing_User_Request__c staffingUserRequest : staffingUserRequestList){
                if(!processInstanceTostaffingUserRequest.values().contains(staffingUserRequest.Id)){
                    staffingUserRequest.Status__c = 'Rejected';
                    staffingUserRequestUpdateList.add(staffingUserRequest);
                }
            }
            if(!staffingUserRequestUpdateList.isEmpty()){
                SObjectAccessDecision decision =  SecurityLibrary.getAccessibleData('Staffing_User_Request__c', staffingUserRequestUpdateList, 'update', true,'NC_StaffingRequestTriggerHandler','checkTotalStaffingUserRequest');
                update decision.getRecords();
            }
        }  
    }
}