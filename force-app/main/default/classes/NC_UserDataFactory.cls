/*
*   Class Name: NC_UserTrigger
*   Description: Trigger on User
*
*   Date            New/Modified         User                 Identifier                Description
*   27/04/2020         New         Shubham Dadhich(mtx)
*/

public with sharing class NC_UserDataFactory {
    
     public static User createUserReccord(String profileName,String lastName,String Email, Boolean isCreate) {
         Id useProfileId = [SELECT Id FROM Profile WHERE Name =: profileName].Id;
         User user = new User(
             ProfileId = useProfileId,
             LastName = lastName,
             Email = Email,
             Username = Email + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
         );
        return user; 
    }

    public static List<User> createUserReccordBulk(String profileName,String lastName,String Email, Boolean isCreate, Integer NumberOfRecords) {
        Id useProfileId = [SELECT Id FROM Profile WHERE Name =: profileName].Id;
        List<User> listOfUser = new List<User>();
        for(Integer i=0; i<NumberOfRecords; i++){
            User user = new User(
                ProfileId = useProfileId,
                LastName = lastName+i,
                Email = Email+i,
                Username = Email + System.currentTimeMillis()+i,
                CompanyName = 'TEST'+i,
                Title = 'title'+i,
                Alias = 'alias'+i,
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US'
            );
            listOfUser.add(user);
        }
        
       return listOfUser; 
   }
}