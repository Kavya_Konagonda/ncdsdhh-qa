/*
*   Class Name: NC_UserTriggerTest
*   Description: User Trigger Handler Test Class
*
*   Date            New/Modified         User                 Identifier                Description
*   27/04/2020         New         Shubham Dadhich(mtx)
*/

@isTest
public with sharing class NC_UserTriggerTest {

    /*
    * Method Name: makeData
    * Description: Data Setup
    * @param:
    * @return void
    */
    @TestSetup
    public static void makeData(){
        List<User> userList = new List<User>();
        userList =  NC_UserDataFactory.createUserReccordBulk(NC_Constant.Profile_StandardUser,'LastNameTest','fn.ln@test.com', false, 5);
        for(User userRecord : userList){
            userRecord.Regional_Center__c = NC_Constant.RegionalCenter_Asheville_Regional_Center;
        }
        INSERT userList;
        
    }
   

    /*
    * Method Name: testUserUpdateRegionalCenter
    * Description: Update Regional Center to Test User Added in Group or Not
    * @param:
    * @return void
    */
    @isTest
    public static void testUserUpdateRegionalCenter(){
        List<User> listOfUser = new List<User>();
        List<GroupMember> groupMemberList = new List<GroupMember>();

        listOfUser = [SELECT Id, Name, Regional_Center__c FROM User WHERE Profile.Name=: NC_Constant.Profile_StandardUser LIMIT 5];
        for(User userRecord : listOfUser){
            userRecord.Regional_Center__c = NC_Constant.RegionalCenter_Charlotte_Regional_Center;
        }
        UPDATE listOfUser;
        groupMemberList = [SELECT Id,Group.DeveloperName FROM GroupMember WHERE UserOrGroupId IN: listOfUser];
        System.assert(groupMemberList.size() > 0);
    }
}