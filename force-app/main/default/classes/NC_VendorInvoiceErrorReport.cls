/**
 * Created by ashishpandey on 14/06/21.
 */

@RestResource(urlMapping='/VendorInvoiceErrorReport/*')
global class NC_VendorInvoiceErrorReport {

    /*Request
        [
           {
              "Pay_Entity":"26PS",
              "Vendor_Number":"0000XGBSTY",
              "Group_Number":"01",
              "Invoice_Date":"05202021",
              "Invoice_Number":"INV-00021",
              "Error_Message":"Line record not present"
           }
        ]
    */
    @HttpPost
    global static void doPost() {
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        List<Error__c> errorList = new List<Error__c>();

        try{
            CNDSResponseWrapper responseWrapper = new CNDSResponseWrapper();
            List<CNDSRequestWrapper> wrapperObjects = (List<CNDSRequestWrapper>)JSON.deserialize(request.requestbody.toString(), CNDSRequestWrapper.class);
            for(CNDSRequestWrapper wrap :wrapperObjects){
                Error__c error = new Error__c();
                error.Pay_Entity__c= wrap.Pay_Entity;
                error.Vendor_Number__c = wrap.Vendor_Number;
                error.Group_Number__c = wrap.Group_Number;
                error.Invoice_Date__c= wrap.Invoice_Date;
                error.Invoice_Number__c = wrap.Invoice_Number;
                error.Error_Description__c = wrap.Error_Message;
                errorList.add(error);

            }
            if (errorList.size()>0) {
                insert errorList;
                responseWrapper.errorMsg=null;
                responseWrapper.success=true;
                res.responseBody = Blob.valueOf(JSON.serializePretty(responseWrapper));
            }
        }catch (Exception e){
            //system.debug('Error' + e.getMessage());
            excepMessage(res,e);
        }

    }

    private static void excepMessage(RestResponse res , Exception e){
        if(res!=null){
            CNDSResponseWrapper responseWrapper = new CNDSResponseWrapper();
            res.addHeader('Content-Type', 'application/json');
            responseWrapper.recordId=null;
            responseWrapper.success=false;
            responseWrapper.errorMsg=e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(responseWrapper));
            res.statusCode = 400;
        }
    }
    public class CNDSRequestWrapper{
        public String Pay_Entity;
        public String Vendor_Number;
        public String Group_Number;
        public String Invoice_Date;
        public String Invoice_Number;
        public String Error_Message;
    }
    public class CNDSResponseWrapper{
        public String recordId;
        public String errorMsg;
        public Boolean success;
    }

}