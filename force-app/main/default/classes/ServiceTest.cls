/**
* Created by ashishPandey on 05-02-2020.
*/

@RestResource(urlMapping='/ServiceTest/*')
global with sharing class ServiceTest {

    /**API URL: https://ncdsdhh--qa.my.salesforce.com/services/apexrest/ServiceTest?method=methodName******/
    /*Request
     * {
        "ObjectName": "Invoice__c",
        "Operation": "update",
        "records": [{
            "attributes": {
                "type": "Invoice__c"
            },
            "Sent_to_NCAS__c": false,
            "Id": "a0Dr000002YeG4EAK"
        }, {
            "attributes": {
                "type": "Invoice__c"
            },
            "Sent_to_NCAS__c": false,
            "Id": "a0Dr0000002YWz9EAG"
    
        }]
    }*/

    @HttpPost
    global static void doPost() {
       
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        String jsonText = request.params.get('method'); 

        try {

            wrapper wrapperObject = (wrapper)JSON.deserialize(request.requestbody.toString(), wrapper.class);
            List<sObject> listObject = wrapperObject.records;
            Database.SaveResult[] srList;
            if(wrapperObject.Operation =='Create'){
                srList = Database.insert(listObject, false);
            }

            if(wrapperObject.Operation =='Update'){
                 srList = Database.update(listObject, false);
                
            }
            if(wrapperObject.Operation =='Upsert'){
               //srList = Database.upsert(listObject, false);
            }
            
            // Iterate through each returned result
            List<responseWrapper> wrapperList = new List<responseWrapper>();
            
            for(Integer i=0;i<srList.size();i++){
                responseWrapper wrap = new responseWrapper();
                if (srList.get(i).isSuccess()){
                    srList.get(i).getId();
                    wrap.recordId=srList.get(i).getId();
                }else if (!srList.get(i).isSuccess()){
                    // DML operation failed
                    Database.Error error = srList.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    listObject.get(i);//failed record from the list
                    wrap.recordId=listObject.get(i).Id;
                    wrap.errorMsg=error.getMessage();
                 }
                wrapperList.add(wrap);
            }
            
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(JSON.serializePretty(wrapperList));
            res.statusCode = 201;
        } catch (Exception e) {
            excepMessage(res,e);
        }
    }
    private static void excepMessage(RestResponse res , Exception e){
        if(res!=null){
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf('{ "errorMsg" : "' + e.getMessage() + '" }');
            res.statusCode = 400;
        }
    }
    public class responseWrapper{
        public String recordId;
        public String errorMsg;
    }
    public class wrapper{
        public List<sObject> records;
        public String ObjectName ;
        public String Operation  ;
    }


}