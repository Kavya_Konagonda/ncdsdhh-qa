public class TestDataUtility {

    public static Case createCase() {
		return null;
    }
    
    public static Account createVendor(Boolean isInsert) {
		Account accObj = new Account();
        accObj.name = 'Test Vendor';
        accObj.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        accObj.Equipment_Provider__c = true;
        accObj.Vendor_Name__c = 'Test';
        accObj.Tax_Id_Number__c = '123456789';
        accObj.X1099_Payee_Name__c = 'Test';
        accObj.Remit_to_Addr_Ln_1__c = 'Test';
        accObj.Remit_to_State_Cd__c = 'NY';
        accObj.Remit_to_Postal_Cd__c = '12345';
        accObj.W9_Entity_Type__c = 'Corporation - Any';
        accObj.Electronic_Pay_Vendor__c = 'No';
        accObj.ShippingStreet = 'Test';
        accObj.ShippingCity = 'Test';
        accObj.ShippingStateCode = 'NY';
        accObj.ShippingCountryCode = 'US';
        accObj.ShippingPostalCode = '12345';
        accObj.BillingStreet = 'Test';
        accObj.BillingCity = 'Test';
        accObj.BillingStateCode = 'NY';
        accObj.BillingCountryCode = 'US';
        accObj.BillingPostalCode = '12345';
        if(isInsert) {
         	insert accObj;   
        }
        return accObj;
    }
    
    public static Contact createConsumer(Boolean isInsert) {
        Contact consumer = new Contact();
        consumer.firstName = 'Test';
        consumer.lastName = 'Test';
        consumer.Gender__c = 'Male';
        consumer.Birthdate = Date.valueOf('2000-11-20');
        consumer.County__c = 'Ashe';
        consumer.Hearing_Disability__c = 'Deaf';
        consumer.MailingStreet = 'Test';
        consumer.MailingCity = 'Test';
        consumer.MailingStateCode = 'NY';
        consumer.MailingCountryCode = 'US';
        consumer.MailingPostalCode = '12345';
        consumer.OtherStreet = 'Test';
        consumer.OtherCity = 'Test';
        consumer.OtherStateCode = 'NY';
        consumer.OtherCountryCode = 'US';
        consumer.OtherPostalCode = '12345';
        consumer.Ethnicity__c = 'Hispanic/Latino';
        consumer.Hispanic_Ethnicity__c = 'Cuban';
        consumer.Social_Security_Number__c = '1234';
        consumer.Proof_of_Income_Uploaded__c = true;
        consumer.Proof_of_Income_Provided_By__c = 'Medicaid';
        consumer.Proof_Of_Residency_Uploaded__c = true;
        consumer.Proof_Of_Residency_Provided_By__c = 'Bank Statement';
        consumer.Income_Range__c = '$0 - $9999';
        consumer.Disability_Determination_form_Uploaded__c = true;
        consumer.Voter_Registration_Form_Uploaded__c = true;
        consumer.Attended_1_1_Consultation__c = true;
        consumer.Attended_Information_Session__c = true;
        
        if(isInsert){
            insert consumer;
        }
        return consumer;
    }
    
    public static Case createNDBEDPCase(Boolean isInsert) {
        Case caseObj = new Case();
        caseObj.contactId = createConsumer(true).id;
        caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NDBEDP').getRecordTypeId();
        caseObj.Request_Audience__c = 'Consumer';
        caseObj.Request_Type__c = 'NDBEDP';
        caseObj.Equipment_Types__c = 'NDBEDP';
        if(isInsert) {
            insert caseObj;
        }
        return caseObj;
    }
    
    public static Case createNDBEDPCase(Boolean isInsert, String contactId) {
        Case caseObj = new Case();
        caseObj.contactId = contactId;
        caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('NDBEDP').getRecordTypeId();
        caseObj.Request_Audience__c = 'Consumer';
        caseObj.Request_Type__c = 'NDBEDP';
        caseObj.Equipment_Types__c = 'NDBEDP';
        if(isInsert) {
            insert caseObj;
        }
        return caseObj;
    }
    
    public static C1_Check_Statement__c createCheckStatement(Boolean createVendor, String vendorId, Boolean isInsert) {
        if(createVendor) {
            vendorId = createVendor(true).id;
        }
        C1_Check_Statement__c checkStatement = new C1_Check_Statement__c();
        checkStatement.Vendor__c = vendorId;
        if(isInsert) {
         	insert checkStatement;   
        }
        return checkStatement;
    }
    
    public static ContentVersion createContentVersion(Boolean checkStatement) {
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        cv.Check_Statement__c = checkStatement;
        insert cv;
        return cv;
    }
    
    public static void createContentDocumentLink(Id parentId, Boolean checkStatement) {
        ContentVersion cv = createContentVersion(checkStatement);
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = parentId;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        insert cdl;
    }
}