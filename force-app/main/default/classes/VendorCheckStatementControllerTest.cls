@isTest
public class VendorCheckStatementControllerTest {

    //static C1_Check_Statement__c checkStatement = new C1_Check_Statement__c();
    
    @testSetup static void setup() {
        //checkStatement = TestDataUtility.createCheckStatement(true, null, true);
    }
    
    static testmethod void markStatementPrintedAndMailedTest() {
        C1_Check_Statement__c checkStatement = TestDataUtility.createCheckStatement(true, null, true);
        C1_Check_Statement__c result = VendorCheckStatementController.markStatementPrintedAndMailed(checkStatement.id, true, true);
        System.assertEquals(true, result.Printed__c);
        System.assertEquals(true, result.Mailed__c);
    }
    
    static testmethod void retrieveCheckStatementsTest() {
        C1_Check_Statement__c checkStatement = TestDataUtility.createCheckStatement(true, null, false);
        checkStatement.Printed__c = true;
        checkStatement.Mailed__c = true;
        insert checkStatement;
        TestDataUtility.createContentDocumentLink(checkStatement.id, true);
        Integer month = Date.today().month();
        Integer year = Date.today().year();
        Map<String,Object> resultMap = VendorCheckStatementController.retrieveCheckStatements(checkStatement.Vendor__c, 0, 5, null, month, year, true, true, false);
        System.assertEquals(1, Integer.valueOf(resultMap.get('count')));
    }
}