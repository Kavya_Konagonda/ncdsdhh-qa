@isTest
public class WebServiceMock {
    
    public class GetEquipmentRequestStatusMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest request) {
            
            NC_NDBEDPEquipmentCatalog.MatRequestStatus_TtMatRequestLine ttMatRequestLine = new NC_NDBEDPEquipmentCatalog.MatRequestStatus_TtMatRequestLine();
            ttMatRequestLine.BibRecObj = 'TEST';
            ttMatRequestLine.LastActionCode = 'TEST';
            ttMatRequestLine.ActionCodeDesc = 'TEST';
            ttMatRequestLine.LineNum = 'TEST';
            ttMatRequestLine.LineStatus = 'TEST';
            ttMatRequestLine.LineStatusDate = System.today() + 'TEST';
            ttMatRequestLine.MatRequestLineObj = 'TEST';
            ttMatRequestLine.QtyOrdered = '1';
            
            NC_NDBEDPEquipmentCatalog.MatRequests matRequest = new NC_NDBEDPEquipmentCatalog.MatRequests();
            matRequest.MatReqStatus = 'cancelled';
            matRequest.MatReqStatusDate = SYSTEM.today() + '';
            matRequest.MatReqType = 'TEST';
            matRequest.MatRequestID = 'TEST';
            matRequest.MatRequestObj = 'TEST';
            matRequest.ttMatRequestLine = new List<NC_NDBEDPEquipmentCatalog.MatRequestStatus_TtMatRequestLine>{ttMatRequestLine};
            
            NC_NDBEDPEquipmentCatalog.MatRequestStatusResponse matRequestStatusResp = new NC_NDBEDPEquipmentCatalog.MatRequestStatusResponse();
            matRequestStatusResp.STATUS = 200;
            matRequestStatusResp.ERROR_DESCRIPTION = '';
            matRequestStatusResp.count = 0;
            matRequestStatusResp.hasMoreResults = false;
            matRequestStatusResp.matRequests = new List<NC_NDBEDPEquipmentCatalog.MatRequests>{matRequest};
            
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(JSON.serialize(matRequestStatusResp));
            response.setStatusCode(200);
            return response; 
        }
    }

}