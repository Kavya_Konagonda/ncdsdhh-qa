/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 06-29-2021
 * @last modified by  : kavya.konagonda@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   06-28-2021   kavya.konagonda@mtxb2b.com   Initial Version
**/
public without sharing class nc_DSDHHUnsubscribeController {
    
    @AuraEnabled
    public static Contact getApplicationRecords(String contactId){
       
        // Contact contactRecord = new Contact();
        // if(String.isNotBlank(contactId)){
        //     list<Contact> contactList = [SELECT Id,Communications_Type__c ,Why_You_Unsubscribed__c,
        //                                 LastName ,Hearing_Disability__c , Other_Reason_For_Unsubscribe__c
        //                                 FROM Contact WHERE Id =: contactId WITH SECURITY_ENFORCED];
        //     contactRecord = contactList[0];
        // }
        //return contactRecord;
        String conId=NC_CryptoEncryption.decodeString(contactId);
        Contact contactRecord = new Contact();
        if(String.isNotBlank(conId)){
            list<Contact> contactList = [SELECT Id ,Communications_Type__c ,Why_You_Unsubscribed__c,
                                        LastName , Other_Reason_For_Unsubscribe__c
                                        FROM Contact WHERE Id =: conId WITH SECURITY_ENFORCED];
            contactRecord = contactList[0];
        }
        return contactRecord;
    }

    @AuraEnabled
    public static void saveData( contact contactRecord  ){
        //SObjectAccessDecision decision =  SecurityLibrary.getAccessibleData('Contact', new List<contact>{contactRecord}, 'update', true, 'nc_DSDHHUnsubscribeController', 'saveData');
                //update decision.getRecords();
     if(Schema.sObjectType.Contact.isUpdateable())
     {
        update contactRecord; 
     }
         
    }

    @AuraEnabled
    public static List<SelectOptionWrapper> fatchMultiPicklist(String objectName, String fieldName){
        List<SelectOptionWrapper> opts = new List<SelectOptionWrapper>();
        
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry pickListVal : ple){
            opts.add( new SelectOptionWrapper( pickListVal.getValue(), pickListVal.getLabel())  );
        }    
        
        return opts;
    }

    public class SelectOptionWrapper{
        @AuraEnabled public string value;
        @AuraEnabled public string label;
        
        public SelectOptionWrapper(string value, string label){
            this.value = value;
            this.label = label;
        }
    }
}