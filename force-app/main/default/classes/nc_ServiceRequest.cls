public with sharing class nc_ServiceRequest {

    @AuraEnabled(cacheable=true)
    public static Boolean checkNDBEDP(String recordId){
        List<Case>parentCaseList = new List<Case>();
       
        if(String.isNotBlank(recordId)){
            parentCaseList = [SELECT Id, CaseNumber FROM Case WHERE Id=:recordId AND recordType.DeveloperName = 'NDBEDP' AND Status = 'Approved' WITH SECURITY_ENFORCED];
            if(parentCaseList.size()>0){
                System.debug('true');
                return true;
               
            }
            else{
                System.debug('false');
                return false;
                
            }
        }
        System.debug('false');
        return false;
    }

    @AuraEnabled(cacheable=true)
    public static Boolean checkUserProfile(){
        User use=[SELECT Id,Profile.Name  FROM User
                     WHERE id = :UserInfo.getUserId() AND Profile.Name!=null WITH SECURITY_ENFORCED];
        if(use.Profile.Name == 'NDBEDP Assessor'){
            return false;
        }
        else{
            return true;
        }
    }
    @AuraEnabled(cacheable=true)
    public static String getParentCaseNumber(String recordId){
        String parentCase='';
        if(String.isNotBlank(recordId)){
            parentCase = [SELECT Id, CaseNumber FROM Case WHERE Id=:recordId].CaseNumber;
            return parentCase;
    }
    return null;
}
@AuraEnabled(cacheable=true)
public static Case getParentCaseNumberId(String recordId){
    Case parentCaseId=new Case();
    if(String.isNotBlank(recordId)){
        parentCaseId = [SELECT Id, CaseNumber,ContactId FROM Case WHERE Id=:recordId WITH SECURITY_ENFORCED];
        return parentCaseId;
}
return null;
}
    @AuraEnabled
    public static Case saveData(String dataObj){
        try {
            RequestDataWrapper requestWrapper = (RequestDataWrapper) JSON.deserialize(dataObj, RequestDataWrapper.class);
            System.debug('requestWrapper data is '+requestWrapper);
            Case caseItem = requestWrapper.RequesDataHandler();
            System.debug('caseItem'+caseItem);
            //insert caseItem;
            return NC_ServiceRequestHelper.saveData(caseItem);
        }
        catch (Exception e) {
              System.debug('errorr'+e.getMessage()+e.getLineNumber());  
              System.debug('msg'+e.getStackTraceString());
              throw new AuraHandledException('errorr'+e.getMessage()+e.getLineNumber());  
        }

    }

    public class RequestDataWrapper{
        @AuraEnabled public string requestType;
        @AuraEnabled public string requestAudience;
        @AuraEnabled public string requestSubType;
        @AuraEnabled public string ContactId;
        @AuraEnabled public string parentCaseId;
       // @AuraEnabled public string parentCaseId;

        public Case RequesDataHandler() {
            Case caseValue = new Case();
            caseValue.ContactId=contactId;
            if(requestSubType == 'Follow-up'){
            caseValue.recordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('NDBEDP Follow-up').getRecordTypeId();
            }
            else if(requestSubType == 'Additional Part'){
                caseValue.recordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('NDBEDP Additional Parts').getRecordTypeId();
            }
            else if(requestSubType == 'Repair Request'){
                caseValue.recordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('NDBEDP Repair Request').getRecordTypeId();
            }
            else if(requestSubType == 'Additional Training'){
                caseValue.recordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('NDBEDP Additional Training').getRecordTypeId();
            }
            caseValue.Request_Type__c=requestType;
            caseValue.Request_Sub_type__c=requestSubType;
            caseValue.Request_Audience__c=requestAudience;
            caseValue.Parent_NDBEDP__c = parentCaseId;
            caseValue.Status = 'Submitted';
            return caseValue;
    }
    
}
}