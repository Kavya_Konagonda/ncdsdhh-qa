import {
  LightningElement,
  api,
  track
} from 'lwc';
import fetchPicklist from '@salesforce/apex/Assess.fetchPicklist';
import saveData from '@salesforce/apex/Assess.saveData';
// import getConsumerName from '@salesforce/apex/Assess.getConsumerName';
import getConsumerNameFromRequest from '@salesforce/apex/Assess.getConsumerNameFromRequest';
// import getConsumerEmailFromRequest from '@salesforce/apex/Assess.getConsumerEmailFromRequest';
import Attestation_Label_1 from '@salesforce/label/c.Attestation_Label_1';
import Verification_of_Disability from '@salesforce/label/c.Verification_of_Disability';
import Verification_of_Disability_one from '@salesforce/label/c.Verification_of_Disability_one';
import Verification_of_Disability_two from '@salesforce/label/c.Verification_of_Disability_two';
import Verification_of_Disability_three from '@salesforce/label/c.Verification_of_Disability_three';
import Income_Eligibility from '@salesforce/label/c.Income_Eligibility';
import Income_Eligibility_two from '@salesforce/label/c.Income_Eligibility_two';


import logo from '@salesforce/resourceUrl/NCDSDHHLOGO';
import {
  ShowToastEvent
} from 'lightning/platformShowToastEvent';


export default class Assess extends LightningElement {
  @track AssesmentObj = {};
  @api recordId;
  @api recordContactId;
  @track value = [];
  @track consumerValue = '';
  @track consumerValueNew='';
  @track consumerEmail='';
  @track consumerPhone='';
  @track picklistOptions;
  @track picklistOptionsvisionLoss;
  @track picklistOptionsOther;
  @track picklistOptionscommunicationSkills;
  @track picklistOptionsConnectivity;
  @track picklistOptionsHearingAssessment;
  @track picklistOptionsVisualAssessment;
  @track picklistOptionsTactileAssessment;
  @track picklistOptionsComputerExperience;
  @track picklistOptionsATUsage;
  @track picklistOptionsComputerUsageExperience;
  @track logoURL = logo;
  @track assesmentId = '';
  @track pcMac=false;

  Verification_of_Disability = Verification_of_Disability;
  Verification_of_Disability_one=Verification_of_Disability_one;
  Verification_of_Disability_two=Verification_of_Disability_two;
  Verification_of_Disability_three=Verification_of_Disability_three;
  Income_Eligibility = Income_Eligibility;
  Income_Eligibility_two=Income_Eligibility_two;
  Attestation_Label_1=Attestation_Label_1;

  connectedCallback() {
    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Hearing_Loss__c"
      })
      .then(res => {
        this.picklistOptions = res;
        console.log('picklist', JSON.stringify(this.picklistOptions));
      }).catch(error => {
        console.error(error);
      });

    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Vision_Loss__c"
      })
      .then(res => {
        this.picklistOptionsvisionLoss = res;
      }).catch(error => {
        console.error(error);
      });

    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Other__c"
      })
      .then(res => {
        this.picklistOptionsOther = res;
      }).catch(error => {
        console.error(error);
      });

    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Communication_Skills__c"
      })
      .then(res => {
        this.picklistOptionscommunicationSkills = res;
      }).catch(error => {
        console.error(error);
      });
    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Connectivity__c"
      })
      .then(res => {
        this.picklistOptionsConnectivity = res;
      }).catch(error => {
        console.error(error);
      });
    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Hearing_Assessment__c"
      })
      .then(res => {
        this.picklistOptionsHearingAssessment = res;
      }).catch(error => {
        console.error(error);
      });
    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Visual_Assessment__c"
      })
      .then(res => {
        this.picklistOptionsVisualAssessment = res;
      }).catch(error => {
        console.error(error);
      });

    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Tactile_Assessment__c"
      })
      .then(res => {
        this.picklistOptionsTactileAssessment = res;
      }).catch(error => {
        console.error(error);
      });


    fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Level_of_Computer_Experience__c"
      })
      .then(res => {
        this.picklistOptionsComputerExperience = res;
      }).catch(error => {
        console.error(error);
      });

      fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "AT_Usage__c"
      })
      .then(res => {
        this.picklistOptionsATUsage = res;
      }).catch(error => {
        console.error(error);
      });

      fetchPicklist({
        objectName: "Assessment__c",
        fieldName: "Computer_usage_experience__c"
      })
      .then(res => {
        this.picklistOptionsComputerUsageExperience = res;
      }).catch(error => {
        console.error(error);
      });

    // getConsumerName({
    //     contactId: this.recordId
    //   })
    //   .then(res => {
    //     this.consumerValue = res;
    //   }).catch(error => {
    //     console.error(error);
    //   });

      getConsumerNameFromRequest({
        requestId: this.recordId
      })
      .then(res => {
        this.consumerValueNew = res.Contact.Name;
        this.consumerEmail = res.ContactEmail;
        this.consumerPhone=res.Contact.MobilePhone;
        this.consumerAddress=res.Contact.MailingStreet +"\n"+res.Contact.MailingCity+" "+res.Contact.MailingState+" "+res.Contact.MailingPostalCode+"\n"+res.Contact.MailingCountry;
      }).catch(error => {
        console.error(error);
      });

      // getConsumerEmailFromRequest({
      //   requestId: this.recordId
      // })
      // .then(res => {
      //   this.consumerEmail = res;
      // }).catch(error => {
      //   console.error(error);
      // });
  }
  // fetchFieldsSave(){

  //     fetchFields({
  //         assId: this.assesmentId

  //     })
  //     .then(res => {
  //         this.AssesmentObj = res;
  //     }).catch(error => {
  //         console.error(error);
  //     });

  // }

  handleChange(event) {
    // if(event.target.value == 'Computer usage experience'){
    //   this.pcMac=true;
    // }
    this.AssesmentObj[event.target.name] = event.target.value;

  }

  handleClick() {
    //this.AssesmentObj.contactId = this.recordId;
    this.AssesmentObj.requestId = this.recordId;

    saveData({
        dataObj: JSON.stringify(this.AssesmentObj)
      })
      .then(res => {
        // this.assesmentId = res
        this.showToastMessage('success', 'Assessment saved Sucessfully', 'success');
        let value = true;
        const valueChangeEvent = new CustomEvent("valuechange", {
          detail: {
            value
          }
        });
        // Fire the custom event
        this.dispatchEvent(valueChangeEvent);
      }).catch(error => {
        let message=error.body||error.body.message;
        this.showToastMessage('error', message, 'error');
        console.log(JSON.stringify(error));
      });
  }

  showToastMessage(title, message, variant) {
    const event = new ShowToastEvent({
      "title": title,
      "message": message,
      "variant": variant,
    });
    this.dispatchEvent(event);
  }
}