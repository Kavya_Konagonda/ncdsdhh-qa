import { LightningElement,track } from 'lwc';
import getCasesFromAccount from '@salesforce/apex/AssessorResult.getCasesFromAccount';
import { NavigationMixin } from 'lightning/navigation';

export default class NC_AcceptInvitations extends NavigationMixin(LightningElement) {

    @track caseList;
    @track showCase=false;
    connectedCallback(){
        this.getvalues();
    }
    getvalues(){
    getCasesFromAccount({})
    .then(res => {
        //let result=res;
        if(res.length==0){
            this.showCase=false;
        }else{
            this.showCase=true;
        }
        this.caseList=[];
        for (let i = 0; i < res.length; i++) {
            this.caseList.push(res[i]);
          }
    }).catch(error => {
             console.error(error);
          });
    }
    getInvitation(event){
        console.log('event.target.Id',event.target.dataset.value);
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: 'AssessorAcceptance__c'
            },
            state: {
                Id: event.target.dataset.value
            }
        });
    }
}