import { LightningElement } from 'lwc';

export default class NC_CreateCommunicationAccessRequest extends LightningElement {
    handleClick(){
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Communication_Access_Request__c',
                actionName: 'new'
            }
        });
    }
}