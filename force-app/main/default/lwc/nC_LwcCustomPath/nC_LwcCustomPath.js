import { LightningElement,api, track } from 'lwc';
import getPicklistValues from '@salesforce/apex/NC_LwcCustomPathController.getPicklistValues';


export default class NC_LwcCustomPath extends LightningElement {

  @api fieldApiName;
  @api recordId;
  @api recordTypesNames;
  @api seeSuccessSate;
  @track steps = [];
  @track completedCssClass = 'slds-path__item slds-is-complete';

  connectedCallback() {
      getPicklistValues({fieldApiName : this.fieldApiName, 
                         recordId : this.recordId, 
                         recordTypesNames : this.recordTypesNames}).then(
          result=>{
            this.steps = result;
            console.log('Result-->' + JSON.stringify(result));
          }
      ).catch(
          error=>{
            console.log('error --> ',error);
          }
      );
      this.completedCssClass = this.seeSuccessSate ? this.completedCssClass : 'slds-path__item slds-is-incomplete';
  }
}