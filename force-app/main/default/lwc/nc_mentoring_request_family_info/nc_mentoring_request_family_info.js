import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Nc_mentoring_request_family_info extends LightningElement {

    @api deafFamilyMembers;
    @api memberRelationship;
    @track familyInformation = {haveDeafFamilyMembers : 'No', memberRelationship : '', relationshipIfOther : ''};
    @track showSpinner = false;
    @track hasFamilyMember = false;
    @track otherSelected = false;

    @api
    getData(){
        return this.familyInformation;
    }
    
    @api
    isValid(){
        let valid = false;
       
        let isAllValid = [...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
            
        if(isAllValid === false){
          return isAllValid;
        }

        isAllValid = [...this.template.querySelectorAll('lightning-dual-listbox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if(isAllValid === false){
                return isAllValid;
        }
        
        isAllValid = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        

        valid = isAllValid;
        console.log('Valid Input : ' + valid);
        return valid;
    }

    handleChange(event){
        if( event.target.name === 'haveDeafFamilyMembers' ){
            this.familyInformation.haveDeafFamilyMembers = event.detail.value;
            this.hasFamilyMember = this.familyInformation.haveDeafFamilyMembers == 'Yes' ? true : false;
        }else if( event.target.name === 'memberRelationship' ){
            this.familyInformation.memberRelationship = event.detail.value.join(',');
            this.otherSelected =  event.detail.value.includes("Other") ;
        }else if(event.target.name === 'relationshipIfOther'){
            this.familyInformation.relationshipIfOther = event.detail.value;
        }
    }

    updateInput(event){
        if(event.target.name === 'relationshipIfOther'){
            event.target.value = event.target.value.trim();
            this.familyInformation.relationshipIfOther = event.target.value;
        }
    }

}