import { LightningElement, api, track } from 'lwc';

export default class Nc_mentoring_request_interpreter_certification extends LightningElement {

    @api haveInterpreterCert;
    @api interpreterCertifications;
    @api assessmentLevelOptions;
    
    @track certificateInformation = {holdInterpreterCert: 'No', RIDNumber: '', assessmentLevel: '', certificateValue: ''};
    @track showCertifications = false;
    @track stateLevelAssessment = false;

    handleChange(event) {
        if(event.target.name == 'haveInterpreterCert') {
            this.certificateInformation.holdInterpreterCert = event.detail.value;
        } else if(event.target.name == 'assessmentLevel') {
            this.certificateInformation.assessmentLevel = event.detail.value;
        }
        
        if(this.certificateInformation.holdInterpreterCert == 'Yes') {
            this.showCertifications = true;
        } else {
            this.showCertifications = false;
        }
    }

    handleCertificationChange(event) {
        var value = event.detail.value;
        this.certificateInformation.certificateValue = value.join(';');
        if(value.includes('Cued Language Transliteration State Level Assessment')) {
            this.stateLevelAssessment = true;
            this.certificateInformation.assessmentLevel = '1';
        } else {
            this.stateLevelAssessment = false;
            this.certificateInformation.assessmentLevel = '';
        }
    }

    handleAssessmentLevel(event) {
        var value = event.detail.value;
        let inputElement = this.template.querySelector('lightning-input[data-id=assessmentLevel]');
        if(value && !isNaN(parseInt(value))) {
            value = parseInt(value);
            if(value < 1 || value > 5) {
                inputElement.setCustomValidity('Invalid input');
            } else {
                this.certificateInformation.assessmentLevel = value;
                inputElement.setCustomValidity('');
            }
        } else {
            inputElement.setCustomValidity('Invalid input');
        }
    }

    @api
    getData(){
        return this.certificateInformation;
    }

    @api
    isValid(){
        let isValid = [...this.template.querySelectorAll('lightning-checkbox-group')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
            
        return isValid;
    }
}