import { LightningElement, api, track } from 'lwc';

export default class Nc_mentoring_request_knowledge_exam extends LightningElement {
    @api takenKnowledge;
    @api passedKnowledge;
    @api planningKnowledge;

    @track knowledgeInformation = {tkenCASLINICOrCDIKnowledgeExam : 'No', passedCASLINICOrCDIKnowledge : 'No', planningCASLINICOrCDIKnowledge: 'No set plans to take the test.'};
    @track showSpinner = false;
    @track examNotTaken = true;

    @api
    getData(){
        return this.knowledgeInformation;
    }

    @api
    isValid(){
        let valid = false;
       
        let isAllValid = [...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
            
        if(isAllValid === false){
          return isAllValid;
        }

        isAllValid = [...this.template.querySelectorAll('lightning-dual-listbox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if(isAllValid === false){
                return isAllValid;
        }
        
        isAllValid = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        

        valid = isAllValid;
        console.log('Valid Input : ' + valid);
        return valid;
    }
    
    handleChange(event){
        if( event.target.name === 'tkenCASLINICOrCDIKnowledgeExam' ){
            this.knowledgeInformation.tkenCASLINICOrCDIKnowledgeExam = event.detail.value;
            this.examNotTaken = event.detail.value === 'No' ? true : false;
        }else if( event.target.name === 'passedCASLINICOrCDIKnowledge' ){
            this.knowledgeInformation.passedCASLINICOrCDIKnowledge = event.detail.value;
        }else if( event.target.name === 'planningCASLINICOrCDIKnowledge' ){
            this.knowledgeInformation.planningCASLINICOrCDIKnowledge = event.detail.value;
        }
    }
}