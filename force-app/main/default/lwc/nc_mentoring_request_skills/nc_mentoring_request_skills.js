import { LightningElement, api, track } from 'lwc';

export default class Nc_mentoring_request_skills extends LightningElement {
    @api informationAssistance;
    @api interpreterSkillDevelopment;
    @api mentoringService;

    @track skillsInformation = {informationAssistance : '', interpretingSkillDevelopment : '', mentoringservices: ''};
    @track showSpinner = false;

    @api
    getData(){
        return this.skillsInformation;
    }
    @api
    isValid(){
        let valid = false;
       
        let isAllValid = [...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
            
        if(isAllValid === false){
          return isAllValid;
        }

        isAllValid = [...this.template.querySelectorAll('lightning-dual-listbox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if(isAllValid === false){
                return isAllValid;
        }
        
        isAllValid = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        

        valid = isAllValid;
        console.log('Valid Input : ' + valid);
        return valid;
    }

    handleChange(event){
        if( event.target.name === 'informationAssistance' ){
            this.skillsInformation.informationAssistance = event.detail.value.join(',');
        }else if( event.target.name === 'interpretingSkillDevelopment' ){
            this.skillsInformation.interpretingSkillDevelopment = event.detail.value.join(',');
        }else if( event.target.name === 'mentoringservices' ){
            this.skillsInformation.mentoringservices = event.detail.value.join(',');
        }
    }
}