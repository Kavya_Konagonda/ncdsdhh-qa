import { LightningElement, api } from 'lwc';

export default class Ncdsdhh_modalwindow extends LightningElement {
    @api name;
    showSpinner = false;

    closeWindow(){
        this.dispatchEvent(new CustomEvent('close'));
    }

    save(){
        this.showSpinner = true;
        this.dispatchEvent(new CustomEvent('save'));
    }

    @api
    disableSpinner() {
        this.showSpinner = false;
    }
}